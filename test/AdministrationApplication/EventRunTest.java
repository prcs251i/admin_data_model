/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AdministrationApplication;

import java.util.ArrayList;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jacob
 */
public class EventRunTest {
    private Date run_Date, testDate;
    private Venue venue, testVenue;
    private ArrayList<Act> performingActsList = new ArrayList();
    private EventRun testEventRun;
    private EventType comedy;
    private Address testAddress;
    
    public EventRunTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        performingActsList = new ArrayList();
        Act SeanLock;
        
        testVenue = new Venue("Plymouth Pavillions", testAddress);
        
        SeanLock = new Act("Sean Lock", "Northern english comedian", comedy);
        performingActsList.add(SeanLock);
        
        testEventRun = new EventRun(02, venue, performingActsList, run_Date, 150);
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetID() {
        System.out.println("Test get Id");
        int expResult = (02);
        assertEquals("Incorrect string stored", expResult, testEventRun.getID());
    }

    @Test
    public void testSetID() {
        System.out.println("Test set Id");
        int expResult = (03);
        testEventRun.setID(expResult);
        assertEquals("Incorrect string stored", expResult, testEventRun.getID());
    }
    
    @Test
    public void testSetNegativeID() {
        System.out.println("Test set negative ID");
        int newID = (-3);
        try {
            testEventRun.setID(newID);
            Assert.fail("Setting a negative ID did not throw an illegal argument exception");
        } catch (IllegalArgumentException ex) {}
    }

    @Test
    public void testGetVenue() {
        System.out.println("Test get venue");
        Venue expResult = (venue);
        assertEquals("Incorrect venue stored", expResult, testEventRun.getVenue());
    }

    @Test
    public void testSetVenue() {
        System.out.println("Test set venue");
        Venue expResult = (testVenue);
        testEventRun.setVenue(expResult);
        assertEquals("Incorrect venue stored", expResult, testEventRun.getVenue());
    }
    
    public void testSetNullVenue() {
        System.out.println("Test set null venue");
        Venue newVenue = null;
        try {
            testEventRun.setVenue(newVenue);
            Assert.fail("Setting a null venue did not throw a null pointer exception");
        } catch (NullPointerException ex) {}
    }

    @Test
    public void testGetActsPerforming() {
        System.out.println("Test get acts performing");
        boolean value = false;
        for(Act objCurrAct : testEventRun.getActsPerforming()) {
            if(objCurrAct.getActName().equals("Sean Lock")) {
                value = true;
            }
        }
        assertTrue(value);
    }

    @Test
    public void testSetActsPerforming() {
        System.out.println("Test set acts performing");
        boolean value = false;
        testEventRun.setActsPerforming(performingActsList);
        for(Act objCurrAct : testEventRun.getActsPerforming()) {
            if(objCurrAct.getActName().equals("Sean Lock")) {
                value = true;
            }
        }
        assertTrue(value);
    }

    @Test
    public void testAddActToEvent() {
        System.out.println("Test add act to an event");
        boolean value = false;
        Act actToAdd;
        actToAdd = new Act("Jimmy Car", "Comedian with frequent use of shock humour", comedy);
        testEventRun.addActToEvent(actToAdd);
        for(Act objCurrAct : testEventRun.getActsPerforming()) {
            if(objCurrAct.getActName().equals("Jimmy Car")) {
                value = true;
            }
        }
        assertTrue(value);
    }
    
    @Test
    public void testAddNullActToEvent() {
        System.out.println("Test add a null act to an event");
        Act actToAdd;
        actToAdd = null;
        try {
            testEventRun.addActToEvent(actToAdd);
            Assert.fail("Adding a null act did not throw a null pointer exception");
        } catch (NullPointerException ex) {}
    }
    
    @Test
    public void testAddDuplicateActToEvent() {
        System.out.println("Test add a duplicate act to an event");
        Act SeanLock;
        SeanLock = new Act("Sean Lock", "Northern english comedian", comedy);
        testEventRun.addActToEvent(SeanLock);
        try {
            testEventRun.addActToEvent(SeanLock);
            Assert.fail("Adding a duplicate act to event did not throw an illegal argument exception");
        } catch (IllegalArgumentException ex) {}
    }

    @Test
    public void testRemoveActFromEventRun() {
        System.out.println("Remove an act from an event");
        boolean value = false;
        Act SeanLock;
        SeanLock = new Act("Sean Lock", "Northern english comedian", comedy);
        testEventRun.addActToEvent(SeanLock);
        testEventRun.removeActFromEventRun(SeanLock);   
        for(Act objCurrAct : testEventRun.getActsPerforming()) {
            if (!objCurrAct.equals(SeanLock)) {
                value = true;
            } else {
                value = false;
                Assert.fail("Act was still in the act list");
                break;
            }
        }
    }
    
    @Test
    public void testRemoveNullActFromEventRun() {
        System.out.println("Remove a null act from an event");
        Act actToRemove;
        actToRemove = null;
        try {
            testEventRun.removeActFromEventRun(actToRemove);
            Assert.fail("Removing a null act did not throw a null pointer exception");
        } catch (NullPointerException ex) {}
    }
    
    @Test
    public void testRemoveNonExistentActFromEvent() {
        System.out.println("Remove a non existent act from an event");
        Act actToRemove;
        actToRemove = new Act("Sean Lock", "Northern english comedian", comedy);
        try {
            testEventRun.removeActFromEventRun(actToRemove);
            Assert.fail("Removing an act not in the list didnt throw an illegal argument exception");
        } catch (IllegalArgumentException ex) {}
    }

    @Test
    public void testGetRunDate() {
        System.out.println("Test get run date");
        Date expResult = (run_Date);
        assertEquals("Incorrect date stored", expResult, testEventRun.getRunDate());
    }

    @Test
    public void testSetRunDate() {
        System.out.println("Test set run date");
        Date expResult = (testDate);
        testEventRun.setRunDate(expResult);
        assertEquals("Incorrect date stored", expResult, testEventRun.getRunDate());
    }

    @Test
    public void testGetDuration() {
        System.out.println("Test get duration");
        int expResult = (150);
        assertEquals("Incorrect integer stored", expResult, testEventRun.getDuration());
    }

    @Test
    public void testSetDuration() {
        System.out.println("Test set duration");
        int expResult = (120);
        testEventRun.setDuration(expResult);
        assertEquals("Incorrect integer stored", expResult, testEventRun.getDuration());
    }
    
    @Test
    public void testSetNegativeDuration() {
        System.out.println("Test set negative duration");
        int newDuration = -120;
        try {
            testEventRun.setDuration(newDuration);
            Assert.fail("Setting a negative duration did not throw an illegal argument exception");
        } catch (IllegalArgumentException ex) {}
    }
    
}
