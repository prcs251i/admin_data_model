/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AdministrationApplication;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jacob
 */
public class TicketTest {
    private Venue testVenue;
    private Tier testTier, testTier2;
    private Ticket testTicket;
    
    public TicketTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        testTier = new Tier("Tier A", true, 90, 9, 10);
        testTier2 = new Tier("Tier b", true, 90, 5, 18);
        testTicket = new Ticket(02, 04, testVenue, testTier){};
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetTicketRef() {
        System.out.println("Test get ticket");
        int expResult = (02);
        assertEquals("Incorrect integer stored", expResult, testTicket.getTicketRef());
    }

    @Test
    public void testSetTicketRef() {
        System.out.println("Test set ticket");
        int expResult = (04);
        testTicket.setTicketRef(expResult);
        assertEquals("Incorrect integer stored", expResult, testTicket.getTicketRef());
    }
    
    @Test
    public void testSetNegativeTicketRef() {
        System.out.println("Test set negative ticket ref");
        int newTicketRef = (-4);
        try {
            testTicket.setTicketRef(newTicketRef);
            Assert.fail("Setting a negative ticketRef did not throw an IllegalArgumentException");
        } catch (IllegalArgumentException ex) {}
    }

    @Test
    public void testGetBookingID() {
        System.out.println("Test get Booking id");
        int expResult = (04);
        assertEquals("Incorrect integer stored", expResult, testTicket.getBookingID());
    }

    @Test
    public void testSetBookingID() {
        System.out.println("Test set Booking id");
        int expResult = (07);
        testTicket.setBookingID(expResult);
        assertEquals("Incorrect integer stored", expResult, testTicket.getBookingID());
    }
    
        @Test
    public void testSetNegativeBookingID() {
        System.out.println("Test set negative booking ID");
        int newBooking = (-4);
        try {
            testTicket.setBookingID(newBooking);
            Assert.fail("Setting a negative booking Id did not throw an IllegalArgumentException");
        } catch (IllegalArgumentException ex) {}
    }

    @Test
    public void testGetAllocatedTier() {
        System.out.println("Test get Allocated tier");
        Tier expResult = (testTier);
        assertEquals("Incorrect integer stored", expResult, testTicket.getAllocatedTier());
    }

    @Test
    public void testSetAllocatedTier() {
        System.out.println("Test set Allocated tier");
        Tier expResult = (testTier2);
        testTicket.setAllocatedTier(expResult);
        assertEquals("Incorrect integer stored", expResult, testTicket.getAllocatedTier());
    }
    
    @Test
    public void testSetNullAllocatedTier() {
        System.out.println("Test set null Allocated tier");
        Tier newTier = null;
        try {
            testTicket.setAllocatedTier(newTier);
            Assert.fail("Setting a null tier did not throw a NullPointerException");
        } catch (NullPointerException ex) {}
    }

    public class TicketImpl extends Ticket {
    }
    
}
