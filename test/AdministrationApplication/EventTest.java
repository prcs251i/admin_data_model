/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AdministrationApplication;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import static org.hamcrest.CoreMatchers.equalTo;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jacob
 */
public class EventTest {
    private EventType comedy, music;
    private Event testEvent;
    private List<EventRun> runList;
    private EventRun testEventRun;
    private Venue venue;
    private ArrayList<Act> performingActsList = new ArrayList();
    private Date run_Date;
    private ArrayList<Review> reviewsList, reviewsList2 = new ArrayList();
    private Review testReview, testReview2, testReview3;
    private Date date;
    
    public EventTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        runList = new ArrayList();
        reviewsList = new ArrayList();
        date = new Date(12/03/2016);
        testReview = new Review(03, 18, date, 5, "Was ok");
        testReview2 = new Review(04, 15, date, 6, "Was ok");
        
        testEventRun = new EventRun(03, venue, performingActsList, run_Date, 150);
        runList.add(testEventRun);
        
        comedy = new EventType("comedy");
        music = new EventType("Music");
        
        testEvent = new Event("Jimmy Car live and laughing", "A comedy show featuring various comedians", comedy, "18", "imageurl.com");
        testEvent.setAverageRating(6.0);
        testEvent.setEventRuns(runList);
        testEvent.addReview(testReview);
        testEvent.addReview(testReview2);
        testEvent.setID(03);
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetID() {
        System.out.println("Test get Id");
        int expResult = (03);
        assertEquals("Incorrect string stored", expResult, testEvent.getID());
    }

    @Test
    public void testSetID() {
        System.out.println("Test set Id");
        int expResult = (05);
        testEvent.setID(expResult);
        assertEquals("Incorrect integer stored", expResult, testEvent.getID());
    }
    
    @Test
    public void setNegativeID() {
        System.out.println("Test set negative ID");
        int newID = (-3);
        try {
            testEvent.setID(newID);
            Assert.fail("Setting a negative ID did not throw an illegal argument exception");
        } catch (IllegalArgumentException ex) {}
    }

    @Test
    public void testGetEventTitle() {
        System.out.println("Test get event title");
        String expResult = ("Jimmy Car live and laughing");
        assertEquals("Incorrect string stored", expResult, testEvent.getEventTitle());
    }

    @Test
    public void testSetEventTitle() {
        System.out.println("Test set Event title");
        String expResult = ("Sean Lock Live");
        testEvent.setEventTitle(expResult);
        assertEquals("Incorrect string stored", expResult, testEvent.getEventTitle());
    }
    
    @Test
    public void testSetEmptyEventTitle() {
        System.out.println("Test set empty title");
        String newTitle = ("");
        try {
            testEvent.setEventTitle(newTitle);
            Assert.fail("Setting an empty title did not throw an illegal argument exception");
        } catch (IllegalArgumentException ex) {}
    }
    
    @Test
    public void testSetNullTitle() {
        System.out.println("Test set null title");
        String newTitle = null;
        try {
            testEvent.setEventTitle(newTitle);
            Assert.fail("Setting a null title did not throw an NullPointerException");
        } catch (NullPointerException ex) {}
    }

    @Test
    public void testGetEventDescription() {
        System.out.println("Test get event description");
        String expResult = ("A comedy show featuring various comedians");
        assertEquals("Incorrect string stored", expResult, testEvent.getEventDescription());
    }

    @Test
    public void testSetEventDescription() {
        System.out.println("Test set Event description");
        String expResult = ("A one man stand up by Sean Lock");
        testEvent.setEventDescription(expResult);
        assertEquals("Incorrect string stored", expResult, testEvent.getEventDescription());
    }
    
    @Test
    public void testSetNullEventDescription() {
        System.out.println("Test set null description");
        String newDesc  = null;
        try {
            testEvent.setEventDescription(newDesc);
            Assert.fail("Setting a null description did not throw an NullPointerException");
        } catch (NullPointerException ex) {}
    }
    
    @Test
    public void testSetEmptyEventDescription() {
        System.out.println("Test set empty description");
        String newDesc  = ("");
        try {
            testEvent.setEventDescription(newDesc);
            Assert.fail("Setting a null description did not throw an IllegalArgumentException");
        } catch (IllegalArgumentException ex) {}
    }

    @Test
    public void testGetEventRuns() {
        System.out.println("Test get event runs");
        boolean value = false;
        for(EventRun objCurrEventRun : testEvent.getEventRuns()) {
            if(objCurrEventRun.getID() == (03)) {
                value = true;
            }
        }
        assertTrue(value);
    }

    @Test
    public void testSetEventRuns() {
        System.out.println("Test set Event Runs");
        boolean value = false;
        testEvent.setEventRuns(runList);
        for(EventRun objCurrEventRun : testEvent.getEventRuns()) {
            if(objCurrEventRun.getID() == (03)) {
                value = true;
            }
        }
        assertTrue(value);
    }

    @Test
    public void testGetEventType() {
        System.out.println("Test get event type");
        EventType expResult = (comedy);
        assertEquals("Incorrect event type stored", expResult, testEvent.getEventType());
    }

    @Test
    public void testSetEventType() {
        System.out.println("Test set event type");
        EventType expResult = (music);
        testEvent.setEventType(music);
        assertEquals("Incorrect event type stored", expResult, testEvent.getEventType());
    }
    
    @Test
    public void testSetNullEventType() {
        System.out.println("Test set null EventType");
        EventType newEventType  = null;
        try {
            testEvent.setEventType(newEventType);
            Assert.fail("Setting a null event type did not throw an NullPointerException");
        } catch (NullPointerException ex) {}
    }

    @Test
    public void testCalculateAverageRating() {
        System.out.println("Test Calculate average rating");
        double expResult = (5.5);
        assertThat("Incorrect int calculated", expResult, equalTo(testEvent.calculateAverageRating()));
    }

    @Test
    public void testGetAverageRating() {
        System.out.println("Test Calculate average rating");
        double expResult = (6.0);
        assertThat("Incorrect int calculated", expResult, equalTo(testEvent.getAverageRating()));
    }

    @Test
    public void testSetAverageRating() {
        System.out.println("Test get average rating");
        double expResult = (8.0);
        testEvent.setAverageRating(expResult);
        assertThat("Incorrect test get average rating", expResult, equalTo(testEvent.getAverageRating()));
    }
    
    
    @Test
    public void testAddDuplicateReview() {
        System.out.println("Test add duplicate review");
        Review testReview;
        testReview = new Review(03, 18, date, 5, "Was ok");
        testEvent.addReview(testReview);
        try {
            testEvent.addReview(testReview);
            Assert.fail("Adding a duplicate review did not throw an illegal argument exception");
        } catch (IllegalArgumentException ex) {}
    }
    
    @Test
    public void testAddNullReview() {
        System.out.println("Test add null review");
        Review testReview;
        testReview = null;
        try {
            testEvent.addReview(testReview);
            Assert.fail("Adding a duplicate review did not throw an null pointer exception");
        } catch (NullPointerException ex) {}
    }
    
    @Test
    public void testRemoveReview() {
        System.out.println("Test remove review");
        boolean value = false;
        Review reviewToRemove;
        reviewToRemove = new Review(04, 39, date, 9, "Hilarious");
        testEvent.addReview(reviewToRemove);
        testEvent.removeReview(reviewToRemove);
        for(Review objCurrReview : testEvent.getReviewsList()) {
            if(objCurrReview.getReviewID() != (04)) {
                value = true;
            }
        }
        assertTrue(value);
    }
    
    @Test
    public void testRemoveNonExistentReview() {
        System.out.println("Test remove a non existent review from an event");
        Review reviewToRemove;
        reviewToRemove = new Review(04, 39, date, 9, "Hilarious");
        try {
            testEvent.removeReview(reviewToRemove);
            Assert.fail("Removing a review not in the list didnt throw an illegal argument exception");
        } catch (IllegalArgumentException ex) {}
    }
    
    @Test
    public void testRemoveNullReview() {
        System.out.println("Test remove a null review from an event");
        Review reviewToRemove;
        reviewToRemove = null;
        try {
            testEvent.removeReview(reviewToRemove);
            Assert.fail("Removing a null review did not throw an null pointer exception");
        } catch (NullPointerException ex) {}
        
    }

    @Test
    public void testGetAgeRating() {
        System.out.println("Test get age rating");
        String expResult = "18";
        assertEquals("Incorrect string stored", expResult, testEvent.getAgeRating());
    }

    @Test
    public void testSetAgeRating() {
        System.out.println("Test set age rating");
        String expResult = "15";
        testEvent.setAgeRating(expResult);
        assertEquals("Incorrect string stored", expResult, testEvent.getAgeRating());
    }
    
    @Test
    public void testSetNullAgeRating() {
        System.out.println("Test set null age rating");
        String newAgeRating = null;
        try {
            testEvent.setAgeRating(newAgeRating);
            Assert.fail("Setting a null age rating did not throw a null pointer exception");
        } catch (NullPointerException ex) {}
    }
    
    @Test
    public void testSetEmptyAgeRating() {
        System.out.println("Test set empty age rating");
        String newAgeRating = "";
        try {
            testEvent.setAgeRating(newAgeRating);
            Assert.fail("Setting an empty age rating did not throw a illegal argument exception");
        } catch (IllegalArgumentException ex) {}
    }

    @Test
    public void testGetImageString() {
        System.out.println("Test get image string");
        String expResult = "imageurl.com";
        assertEquals("Incorrect string stored", expResult, testEvent.getImageString());
    }

    @Test
    public void testSetImageString() {
        System.out.println("Test set image string");
        String expResult = "newimageurl.com";
        testEvent.setImageString(expResult);
        assertEquals("Incorrect string stored", expResult, testEvent.getImageString());
    }
    
    @Test
    public void testSetNullImageString() {
        System.out.println("Test set null image string");
        String newImageString = null;
        try {
            testEvent.setImageString(newImageString);
            Assert.fail("Setting a null image string did not throw a null pointer exception");
        } catch (NullPointerException ex) {}
    }
    
    @Test
    public void testSetEmptyImageString() {
        System.out.println("Test set empty image string");
        String newAgeRating = "";
        try {
            testEvent.setAgeRating(newAgeRating);
            Assert.fail("Setting an empty image string did not throw a illegal argument exception");
        } catch (IllegalArgumentException ex) {}
    }
}
