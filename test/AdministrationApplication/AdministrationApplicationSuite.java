/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AdministrationApplication;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author Jacob
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({AdministrationApplication.ActTest.class, AdministrationApplication.AddressTest.class, AdministrationApplication.BookingTest.class, 
    AdministrationApplication.CustomerTest.class, AdministrationApplication.EventRunTest.class, AdministrationApplication.EventTypeTest.class,
    AdministrationApplication.EventTest.class, AdministrationApplication.PersonTest.class, AdministrationApplication.PriceTest.class, 
    AdministrationApplication.ReviewTest.class, AdministrationApplication.SeatedTicketTest.class,AdministrationApplication.TicketTest.class, 
    AdministrationApplication.TierTest.class, AdministrationApplication.VenueTest.class})

public class AdministrationApplicationSuite {
    
    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
}
