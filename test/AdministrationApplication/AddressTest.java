/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AdministrationApplication;

import AdministrationApplication.Address;
import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jacob
 */
public class AddressTest {
    private Address testAddress;
    
    public AddressTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        testAddress = new Address("Millbay road", " ", "Plymouth", "Devon", "PL1 3LF");
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetAddressLine1() {
        System.out.println("Test get address line 1");
        String expResult = ("Millbay road");
        assertEquals("Incorrect string stored", expResult, testAddress.getAddressLine1());
    }

    @Test
    public void testSetAddressLine1() {
        System.out.println("Test set address line 1");
        String expResult = ("Hillsborough ave");
        testAddress.setAddressLine1(expResult);
        assertEquals("Incorrect string stored", expResult, testAddress.getAddressLine1());
    }
    
    @Test
    public void testSetEmptyAddressLine1() {
        System.out.println("Set empty address line 1");
        String newAddressLine1 = "";
        try {
            testAddress.setAddressLine1(newAddressLine1);
            Assert.fail("Setting a empty string for Address line 1 did not throw an Illegal argument exception");
        } catch (IllegalArgumentException ex) {}
    }
    
    @Test
    public void testSetNullAddressLine1() {
        System.out.println("Set null address line 1");
        String newAddressLine1 = null;
        try {
            testAddress.setAddressLine1(newAddressLine1);
            Assert.fail("Setting a null value for Address line 1 did not throw an Illegal argument exception");
        } catch (NullPointerException ex) {}
    }

    @Test
    public void testGetAddressLine2() {
        System.out.println("Test get address line 2");
        String expResult = (" ");
        assertEquals("Incorrect string stored", expResult, testAddress.getAddressLine2());
    }

    @Test
    public void testSetAddressLine2() {
        System.out.println("Test set address line 2");
        String expResult = ("floor 3 apartment 301");
        testAddress.setAddressLine2(expResult);
        assertEquals("Incorrect string stored", expResult, testAddress.getAddressLine2());
    }
    
    @Test
    public void testSetNullAddressLine2() {
        System.out.println("Set null address line 2");
        String newAddressLine2 = null;
        try {
            testAddress.setAddressLine2(newAddressLine2);
            Assert.fail("Setting a null value for Address line 2 did not throw an Illegal argument exception");
        } catch (NullPointerException ex) {}
    }

    @Test
    public void testGetCity() {
        System.out.println("Test get city");
        String expResult = ("Plymouth");
        assertEquals("Incorrect string stored", expResult, testAddress.getCity());
    }

    @Test
    public void testSetCity() {
        System.out.println("Test set city");
        String expResult = ("Bournemouth");
        testAddress.setCity(expResult);
        assertEquals("Incorrect string stored", expResult, testAddress.getCity());
    }
    
    @Test
    public void testSetEmptyCity() {
        System.out.println("Set empty string for city");
        String newCity = "";
        try {
            testAddress.setCity(newCity);
            Assert.fail("Setting a empty string for city did not throw an Illegal argument exception");
        } catch (IllegalArgumentException ex) {}
    }
    
    @Test
    public void testSetNullCity() {
        System.out.println("Set null city");
        String newCity = null;
        try {
            testAddress.setCity(newCity);
            Assert.fail("Setting a null value for city did not throw an Illegal argument exception");
        } catch (NullPointerException ex) {}
    }

    @Test
    public void testGetCounty() {
        System.out.println("Test get county");
        String expResult = ("Devon");
        assertEquals("Incorrect string stored", expResult, testAddress.getCounty());
    }

    @Test
    public void testSetCounty() {
        System.out.println("Test set county");
        String expResult = ("Somerset");
        testAddress.setCounty(expResult);
        assertEquals("Incorrect string stored", expResult, testAddress.getCounty());
    }
    
     @Test
    public void testSetEmptyCounty() {
        System.out.println("Set empty string for county");
        String newCounty= "";
        try {
            testAddress.setCounty(newCounty);
            Assert.fail("Setting a empty string for county did not throw an Illegal argument exception");
        } catch (IllegalArgumentException ex) {}
    }
    
    @Test
    public void testSetNullCounty() {
        System.out.println("Set null county");
        String newCounty = null;
        try {
            testAddress.setCounty(newCounty);
            Assert.fail("Setting a null value for county did not throw an Illegal argument exception");
        } catch (NullPointerException ex) {}
    }

    @Test
    public void testGetPostcode() {
        System.out.println("Test get postcode");
        String expResult = ("PL1 3LF");
        assertEquals("Incorrect string stored", expResult, testAddress.getPostcode());
    }

    @Test
    public void testSetPostcode() {
        System.out.println("Test set postcode");
        String expResult = ("PL5 4NG");
        testAddress.setPostcode(expResult);
        assertEquals("Incorrect string stored", expResult, testAddress.getPostcode());
    }
    
     @Test
    public void testSetEmptyPostCode() {
        System.out.println("Set empty string for postcode");
        String newPostcode = "";
        try {
            testAddress.setPostcode(newPostcode);
            Assert.fail("Setting a empty string for postcode did not throw an Illegal argument exception");
        } catch (IllegalArgumentException ex) {}
    }
    
    @Test
    public void testSetNullPostcode() {
        System.out.println("Set null postcode");
        String newPostcode = null;
        try {
            testAddress.setPostcode(newPostcode);
            Assert.fail("Setting a null value for postcode did not throw an Illegal argument exception");
        } catch (NullPointerException ex) {}
    }
    
}
