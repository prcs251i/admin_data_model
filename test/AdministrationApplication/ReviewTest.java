/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AdministrationApplication;

import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jacob
 */
public class ReviewTest {
    
    private Review testReview;
    private Date date;
    
    public ReviewTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        date = new Date(12/04/2016);
        testReview = new Review(03, 18, date, 5, "Was ok");
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetReviewID() {
        System.out.println("Test get review ID");
        int expResult = (03);
        testReview.setReviewID(expResult);
        assertEquals("Incorrect integer stored", expResult, testReview.getReviewID());
    }

    @Test
    public void testSetReviewID() {
        System.out.println("Test set review ID");
        int expResult = (05);
        testReview.setReviewID(expResult);
        assertEquals("Incorrect integer stored", expResult, testReview.getReviewID());
    }
    
    @Test
    public void testSetNegativeReviewID() {
        System.out.println("Test set negative reivewId");
        int newId = (-3);
        try {
            testReview.setReviewID(newId);
            Assert.fail("Setting a negative reviewId did not throw a IllegalArgumentException");
        } catch (IllegalArgumentException ex) {}
    }

    @Test
    public void testGetUserID() {
        System.out.println("Test get user ID");
        int expResult = (18);
        assertEquals("Incorrect integer stored", expResult, testReview.getUserID());
    }

    @Test
    public void testSetUserID() {
        System.out.println("Test set user ID");
        int expResult = (32);
        testReview.setUserID(expResult);
        assertEquals("Incorrect integer stored", expResult, testReview.getUserID());
    }
    
    @Test
    public void testSetNegativeUserID() {
        System.out.println("Test set negative userId");
        int newId = (-3);
        try {
            testReview.setUserID(newId);
            Assert.fail("Setting a negative userId did not throw a IllegalArgumentException");
        } catch (IllegalArgumentException ex) {}
    }

    @Test
    public void testGetEventID() {
        System.out.println("Test get event ID");
        int expResult = (18);
        testReview.setEventID(expResult);
        assertEquals("Incorrect integer stored", expResult, testReview.getEventID());
    }

    @Test
    public void testSetEventID() {
        System.out.println("Test set event ID");
        int expResult = (28);
        testReview.setEventID(expResult);
        assertEquals("Incorrect integer stored", expResult, testReview.getEventID());
    }
    
    @Test
    public void testSetNegativeEventID() {
        System.out.println("Test set negative EventId");
        int newId = (-3);
        try {
            testReview.setEventID(newId);
            Assert.fail("Setting a negative EventId did not throw a IllegalArgumentException");
        } catch (IllegalArgumentException ex) {}
    }

    @Test
    public void testGetUserRating() {
        System.out.println("Test get user rating");
        int expResult = (5);
        assertEquals("Incorrect integer stored", expResult, testReview.getUserRating());
    }

    @Test
    public void testSetUserRating() {
        System.out.println("Test set user rating");
        int expResult = (8);
        testReview.setUserRating(expResult);
        assertEquals("Incorrect integer stored", expResult, testReview.getUserRating());
    }
    
    @Test
    public void testSetNegativeUserRating() {
        System.out.println("Test set user rating");
        int newRating = (-3);
        try {
            testReview.setUserRating(newRating);
            Assert.fail("Setting a negative user rating did not throw a IllegalArgumentException");
        } catch (IllegalArgumentException ex) {}
    }

    @Test
    public void testGetUserComments() {
        System.out.println("Test get user comments");
        String expResult = ("Was ok");
        assertEquals("Incorrect integer stored", expResult, testReview.getUserComments());
    }

    @Test
    public void testSetUserComments() {
        System.out.println("Test set user comments");
        String expResult = ("meh");
        testReview.setUserComments(expResult);
        assertEquals("Incorrect integer stored", expResult, testReview.getUserComments());
    }
    
    @Test
    public void testSetNullUserComments() {
        System.out.println("Test set null user comments");
        String newUserComments = null;
        try {
            testReview.setUserComments(newUserComments);
            Assert.fail("Setting null user comments did not throw a NullpointerException");
        } catch (NullPointerException ex) {}
    }
    
}
