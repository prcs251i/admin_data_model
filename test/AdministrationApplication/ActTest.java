/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AdministrationApplication;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jacob
 */
public class ActTest {
    private Act testAct;
    private EventType comedy;
    private EventType music;
    
    public ActTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        testAct = new Act("Jimmy Car", "Comedian with frequent use of shock humour", comedy);
        music = new EventType("Comedy");
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetID() {
        System.out.println("Test get ID");
        int expResult = (0112);
        testAct.setID(expResult);
        assertEquals("Incorrect string stored", expResult, testAct.getID());
    }

    @Test
    public void testSetID() {
        System.out.println("Test set ID");
        int expResult = (0432);
        testAct.setID(expResult);
        assertEquals("Incorrect string stored", expResult, testAct.getID());
    }
    
    public void testSetNegativeID() {
        System.out.println("set NEGATIVE Act ID");
        int newID = -47600004;
        try {
            testAct.setID(newID);
            Assert.fail("Setting a negative Act ID did not throw a Illegal argument exception");
        } catch (IllegalArgumentException ex) {} 
    }

    @Test
    public void testGetActName() {
        System.out.println("Test get Name");
        String expResult = ("Jimmy Car");
        assertEquals("Incorrect string stored", expResult, testAct.getActName());
    }

    @Test
    public void testSetActName() {
        System.out.println("Test set Name");
        String expResult = ("Sean Lock");
        testAct.setActName(expResult);
        assertEquals("Incorrect string stored", expResult, testAct.getActName());
    }
    
    @Test
    public void testSetNullActName() {
        System.out.println("set NULL act name");
        String newActName = null;
        try {
            testAct.setActName(newActName);
            Assert.fail("Setting a NULL value for act name did not throw an null pointer exception");
        } catch (NullPointerException ex) {}
    }
    
    @Test
    public void testSetEmptyActName() {
        System.out.println("Set empty act name");
        String newActName = "";
        try {
            testAct.setActName(newActName);
            Assert.fail("Setting a empty string for act name did not throw an Illegal argument exception");
        } catch (IllegalArgumentException ex) {}
    }

    @Test
    public void testGetActDesc() {
        System.out.println("Test get Description");
        String expResult = ("Comedian with frequent use of shock humour");
        assertEquals("Incorrect string stored", expResult, testAct.getActDesc());
    }

    @Test
    public void testSetActDesc() {
        System.out.println("Test set Description");
        String expResult = ("English Comedian and actor");
        testAct.setActDesc(expResult);
        assertEquals("Incorrect string stored", expResult, testAct.getActDesc());
    }
    
    @Test
    public void testSetNullActDesc() {
        System.out.println("set NULL act desc");
        String newActDesc = null;
        try {
            testAct.setActDesc(newActDesc);
            Assert.fail("Setting a NULL value for act desc did not throw an null pointer exception");
        } catch (NullPointerException ex) {}
    }
    
    @Test
    public void testSetEmptyActDesc() {
        System.out.println("Set empty act desc");
        String newActDesc = "";
        try {
            testAct.setActDesc(newActDesc);
            Assert.fail("Setting a empty string for act desc did not throw an Illegal argument exception");
        } catch (IllegalArgumentException ex) {}
    }

    @Test
    public void testGetActType() {
        System.out.println("Test get Act Type");
        EventType expResult = (comedy);
        assertEquals("Incorrect event type stored", expResult, testAct.getActType());
    }

    @Test
    public void testSetActType() {
        System.out.println("Test set Act Type");
        EventType expResult = (music);
        testAct.setActType(expResult);
        assertEquals("Incorrect event type stored", expResult, testAct.getActType());
    }
    
    @Test
    public void testSetNullActType() {
        System.out.println("set NULL act type");
        EventType newActType = null;
        try {
            testAct.setActType(newActType);
            Assert.fail("Setting a NULL value for act type did not throw an null pointer exception");
        } catch (NullPointerException ex) {
        }
    }
    
}
