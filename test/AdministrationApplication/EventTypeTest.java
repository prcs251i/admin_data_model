/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AdministrationApplication;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jacob
 */
public class EventTypeTest {
    private EventType testEventType;
    
    public EventTypeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        testEventType = new EventType("comedy");
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetID() {
        System.out.println("Test get Id");
        int expResult = (01);
        testEventType.setID(expResult);
        assertEquals("Incorrect integer stored", expResult, testEventType.getID());
    }

    @Test
    public void testSetID_int() {
        System.out.println("Test set Id");
        int expResult = (02);
        testEventType.setID(expResult);
        assertEquals("Incorrect integer stored", expResult, testEventType.getID());
    }
    
    @Test
    public void testSetNegativeID() {
        System.out.println("Tes set NEGATIVE Booking ID");
        int newID = -47600004;
        try {
            testEventType.setID(newID);
            Assert.fail("Setting a negative ID did not throw a Illegal argument exception");
        } catch (IllegalArgumentException ex) {} 
    }

    @Test
    public void testGetType() {
        System.out.println("Test get type");
        String expResult = ("comedy");
        assertEquals("Incorrect string stored", expResult, testEventType.getType());
    }

    @Test
    public void testSetType() {
        System.out.println("Test set type");
        String expResult = ("music");
        testEventType.setType(expResult);
        assertEquals("Incorrect integer stored", expResult, testEventType.getType());
    }
    
    @Test
    public void testSetNullEventType() {
        System.out.println("Test set NULL Customer");
        String newType = null;
        try {
            testEventType.setType(newType);
            Assert.fail("Setting a NULL value for event type did not throw an Null Pointer exception");
        } catch (NullPointerException ex) {}
    }
    
    @Test
    public void testSetEmptyEventType() {
        System.out.println("Test set empty string EventType");
        String newType = "";
        try {
            testEventType.setType(newType);
            Assert.fail("Setting an empty string for event type did not throw an Illegal argument exception");
        } catch (IllegalArgumentException ex) {}
    }
    
    
}
