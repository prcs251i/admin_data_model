/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AdministrationApplication;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jacob
 */
public class VenueTest {
    private List<Tier> tierList, testTierList;
    private Venue testVenue;
    private Address testAddress, testAddress2;
    private Tier tierA, tierB, tierC;
    
    public VenueTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        testAddress = new Address("Millbay road", " ", "Plymouth", "Devon", "PL1 3LF");
        testAddress2 = new Address("Millbay road", "3", "Plymouth", "Devon", "PL1 3LF");
        testVenue = new Venue("Plymouth Pavillions", testAddress);
        tierList = new ArrayList();
        
        Tier tierA;
        Tier tierB;
        Tier tierC;
        
        tierA = new Tier("TierA", true, 55, 11, 5);
        tierList.add(tierA);
        
        tierB = new Tier("TierB", true, 45, 11, 4);
        tierList.add(tierB);
        
        tierC = new Tier("TierC", true, 65, 11, 6);
        tierList.add(tierC);
        
        testVenue.setTierList(tierList);
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetVenueID() {
        System.out.println("Test get venue ID");
        int expResult = (03);
        testVenue.setVenueID(expResult);
        assertEquals("Incorrect integer stored", expResult, testVenue.getVenueID());
    }

    @Test
    public void testSetVenueID() {
        System.out.println("Test set venue ID");
        int expResult = (05);
        testVenue.setVenueID(expResult);
        assertEquals("Incorrect integer stored", expResult, testVenue.getVenueID());
    }
    
    @Test
    public void testSetNegativeVenueId() {
        System.out.println("Test set negative venue Id");
        int newVenueId = (-3);
        try {
            testVenue.setVenueID(newVenueId);
            Assert.fail("Setting a negative id didnt throw an illegal argument exception");
        } catch (IllegalArgumentException ex) {}
    }

    @Test
    public void testGetVenueName() {
        System.out.println("Test get venue name");
        String expResult = ("Plymouth Pavillions");
        assertEquals("Incorrect integer stored", expResult, testVenue.getVenueName());
    }

    @Test
    public void testSetVenueName() {
        System.out.println("Test set venue name");
        String expResult = ("Royal Theatre");
        testVenue.setVenueName(expResult);
        assertEquals("Incorrect integer stored", expResult, testVenue.getVenueName());
    }
    
    @Test
    public void testSetEmptyVenueName() {
        System.out.println("Test set empty venue name");
        String newVenueName = ("");
        try {
            testVenue.setVenueName(newVenueName);
            Assert.fail("Setting a empty venue name did not throw an illegal argument exception");
        } catch (IllegalArgumentException ex) {}
    }
    
    @Test
    public void testSetNullVenueName() {
        System.out.println("Test set null venue name");
        String newVenueName = null;
        try {
            testVenue.setVenueName(newVenueName);
            Assert.fail("Setting a null venue name did not throw an NullPointerException");
        } catch (NullPointerException ex) {}
    }

    @Test
    public void testGetVenueAddress() {
        System.out.println("Test get venue address");
        Address expResult = (testAddress);
        assertEquals("Incorrect Address stored", expResult, testVenue.getVenueAddress());
    }

    @Test
    public void testSetVenueAddress() {
        System.out.println("Test set venue address");
        Address expResult = (testAddress2);
        testVenue.setVenueAddress(expResult);
        assertEquals("Incorrect Address stored", expResult, testVenue.getVenueAddress());
    }
    
    @Test
    public void testSetNullVenueAddress() {
        System.out.println("Test set null venue address");
        Address newVenueAddress = null;
        try {
            testVenue.setVenueAddress(newVenueAddress);
            Assert.fail("Setting a null venue address did not throw an NullPointerException");
        } catch (NullPointerException ex) {}
    }

    @Test
    public void testGetCapacity() {
        System.out.println("Test get capacity");
        int expResult = (165);
        assertEquals("Incorrect integer stored", expResult, testVenue.getCapacity());
    }

    @Test
    public void testGetStandingCapacity() {
        System.out.println("Test get standing capacity");
        int expResult = (0);
        assertEquals("Incorrect integer stored", expResult, testVenue.getStandingCapacity());
    }

    @Test
    public void testGetSeatedCapacity() {
        System.out.println("Test get seated capacity");
        int expResult = (165);
        assertEquals("Incorrect integer stored", expResult, testVenue.getSeatedCapacity());
    }

    @Test
    public void testGetTierList() {
        System.out.println("Test get tier list");
        boolean value = false;
        for(Tier objCurrTier : testVenue.getTierList()) {
            if(objCurrTier.getTierName().equals("TierA")) {
                value = true;
            }
        }
        assertTrue(value);
    }

    @Test
    public void testAddTier() {
        System.out.println("Test add a tier to a venue");
        boolean value = false;
        Tier tierD;
        tierD = new Tier("TierD", false, 80, 0, 0);
        testVenue.addTier(tierD);
        for(Tier objCurrTier : testVenue.getTierList()) {
            if(objCurrTier.getTierName().equals("TierD")) {
                value = true;
            }
        }
        assertTrue(value);
    }

    @Test
    public void testAddDuplicateTier() {
        System.out.println("Test add a duplicate tier to a venue");
        Tier tierD;
        tierD = new Tier("TierD", false, 80, 0, 0);
        testVenue.addTier(tierD);
        try {
            testVenue.addTier(tierD);
            Assert.fail("Adding a duplicate venue did not throw a IllegalArgumentException");
        } catch (IllegalArgumentException ex) {}
    }
    
    @Test
    public void testAddNullTier() {
        System.out.println("Test add a null tier to a venue");
        Tier tierD;
        tierD = null;
        try {
            testVenue.addTier(tierD);
            Assert.fail("Adding a duplicate venue did not throw a NullPointerException");
        } catch (NullPointerException ex) {}
        
    }
    
    @Test
    public void testAddTiers() {
        System.out.println("Test add multiple tiers to a venue");
        boolean value = false;
        
        testTierList = new ArrayList();
        
        Tier tierE;
        Tier tierF;
        
        tierE = new Tier("TierE", false, 35, 0, 0);
        testTierList.add(tierE);
        
        tierF = new Tier("TierF", false, 40, 0, 0);
        testTierList.add(tierF);
        
        testVenue.addTiers(testTierList);
        
        for(Tier objCurrTier : testVenue.getTierList()) {
            if(objCurrTier.getTierName().equals("TierE") && objCurrTier.getTierName().equals("TierF")) {
                value = true;
            }
        }
    }

    @Test
    public void testSetTierList() {
        System.out.println("Test set ticket list");
        boolean value = false;
        testVenue.setTierList(tierList);
        for(Tier objCurrTier : testVenue.getTierList()) {
            if(objCurrTier.getTierName().equals("TierA")) {
                value = true;
            }
        }
        assertTrue(value);
    }

    @Test
    public void testRemoveTier() {
        System.out.println("Test remove tier from venue");
        boolean value = false;
        Tier tierB;
        tierB = new Tier("TierB", true, 45, 11, 4);
        testVenue.addTier(tierB);
        testVenue.removeTier(tierB);
        for(Tier objCurrTier : testVenue.getTierList()) {
            if(!objCurrTier.getTierName().equals("TierB")) {
                value = true;
            }
        }
        assertTrue(value);
        
    }

    @Test
    public void testRemoveTiers() {
        System.out.println("Test remove multiple tiers from a venue");
        boolean value = false;
        tierList = new ArrayList();
        testVenue.removeTiers(tierList);
        for(Tier objCurrTier : testVenue.getTierList()) {
            if(!objCurrTier.getTierName().equals("TierA") && 
                    !objCurrTier.getTierName().equals("TierB")) {
                value = true;
            }
        }
        assertTrue(value);
    }
    
    @Test
    public void testRemoveNullTier() {
        System.out.println("Test remove a null tier from a venue");
        Tier tierToRemove;
        tierToRemove = null;
        try {
            testVenue.removeTier(tierToRemove);
            Assert.fail("Removing a null tier did not throw a null pointer exception");
        } catch (NullPointerException ex) {}
    }
    
    @Test
    public void testRemoveNonExistentTier() {
        System.out.println("Test remove a non existent tier from a venue");
        Tier tierToRemove;
        tierToRemove = new Tier();
        try {
            testVenue.removeTier(tierToRemove);
            Assert.fail("Removing a tier not in the list didnt throw an illegal argument exception");
        } catch (IllegalArgumentException ex) {}
    }
    
}
