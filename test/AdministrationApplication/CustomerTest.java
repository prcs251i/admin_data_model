/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AdministrationApplication;

import AdministrationApplication.*;
import java.util.Date;
import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jacob
 */
public class CustomerTest {
    private Customer testCustomer;
    private Date expiryDate, secondaryExpiryDate;
    private Address testAddress, secondaryAddress;
    
    public CustomerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        testAddress = new Address("Millbay road", " ", "Plymouth", "Devon", "PL1 3LF");
        secondaryAddress = new Address("Millbay road", "54c", "Plymouth", "Devon", "PL1 3LF");
        
        testCustomer = new Customer("Steve", "Stevenson", "SS@gmail.com", "678 321", 
                "07450739568", testAddress, "visa", "3256 8764 2345 2318" ,expiryDate, "532", "password");
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetAddress() {
        System.out.println("Test get address");
        Address expResult = (testAddress);
        assertEquals("Incorrect string stored", expResult, testCustomer.getAddress());
    }

    @Test
    public void testSetAddress() {
        System.out.println("Test set address");
        Address expResult = (secondaryAddress);
        testCustomer.setAddress(expResult);
        assertEquals("Incorrect string stored", expResult, testCustomer.getAddress());
    }
    
    @Test
    public void testSetNullAddress() {
        System.out.println("Set null address");
        Address newAddress = null;
        try {
            testCustomer.setAddress(newAddress);
            Assert.fail("Setting a null value for Address did not throw an Illegal argument exception");
        } catch (NullPointerException ex) {}
    }

    @Test
    public void testGetHomeNumber() {
        System.out.println("Test get home number");
        String expResult = ("678 321");
        assertEquals("Incorrect string stored", expResult, testCustomer.getHomeNumber());
    }

    @Test
    public void testSetHomeNumber() {
        System.out.println("Test set home number");
        String expResult = ("538 342");
        testCustomer.setHomeNumber(expResult);
        assertEquals("Incorrect string stored", expResult, testCustomer.getHomeNumber());
    }

    @Test
    public void testGetMobNumber() {
        System.out.println("Test get mobile number");
        String expResult = ("07450739568");
        assertEquals("Incorrect string stored", expResult, testCustomer.getMobNumber());
    }

    @Test
    public void testSetMobNumber() {
        System.out.println("Test set mobile number");
        String expResult = ("07674389474");
        testCustomer.setMobNumber(expResult);
        assertEquals("Incorrect string stored", expResult, testCustomer.getMobNumber());
    }
    
    @Test
    public void testGetCardType() {
        System.out.println("Test get card type");
        String expResult = ("visa");
        assertEquals("Incorrect string stored", expResult, testCustomer.getCardType());
    }

    @Test
    public void testSetCardType() {
        System.out.println("Test set card type");
        String expResult = ("master card");
        testCustomer.setCardType(expResult);
        assertEquals("Incorrect string stored", expResult, testCustomer.getCardType());
    }
    
    @Test
    public void testSetEmptyCardType() {
        System.out.println("Set empty CardType");
        String newCardType = "";
        try {
            testCustomer.setCardType(newCardType);
            Assert.fail("Setting a empty string for CardType did not throw an Illegal argument exception");
        } catch (IllegalArgumentException ex) {}
    }
    
    @Test
    public void testSetNullCardType() {
        System.out.println("Set null CardType");
        String newCardType = null;
        try {
            testCustomer.setCardType(newCardType);
            Assert.fail("Setting a null value for CardType did not throw an Illegal argument exception");
        } catch (NullPointerException ex) {}
    }

    @Test
    public void testGetCardNumber() {
        System.out.println("Test get card number");
        String expResult = ("3256 8764 2345 2318");
        assertEquals("Incorrect string stored", expResult, testCustomer.getCardNumber());
    }

    @Test
    public void testSetCardNumber() {
        System.out.println("Test set card number");
        String expResult = ("4567 8864 8960 2319");
        testCustomer.setCardNumber(expResult);
        assertEquals("Incorrect string stored", expResult, testCustomer.getCardNumber());
    }
    
    @Test
    public void testSetEmptyCardNumber() {
        System.out.println("Set empty CardNumber");
        String newCardNumber = "";
        try {
            testCustomer.setCardNumber(newCardNumber);
            Assert.fail("Setting a empty string for CardNumber did not throw an Illegal argument exception");
        } catch (IllegalArgumentException ex) {}
    }
    
    @Test
    public void testSetNullCardNumber() {
        System.out.println("Set null CardNumber");
        String newCardNumber = null;
        try {
            testCustomer.setCardNumber(newCardNumber);
            Assert.fail("Setting a null value for CardNumber did not throw an Illegal argument exception");
        } catch (NullPointerException ex) {}
    }

    @Test
    public void testGetExpiryDate() {
        System.out.println("Test get expiry date");
        Date expResult = (expiryDate);
        assertEquals("Incorrect date stored", expResult, testCustomer.getExpiryDate());
    }

    @Test
    public void testSetExpiryDate() {
        System.out.println("Test set expiry date");
        Date expResult = (secondaryExpiryDate);
        testCustomer.setExpiryDate(expResult);
        assertEquals("Incorrect date stored", expResult, testCustomer.getExpiryDate());
    }

    @Test
    public void testGetSecurityCode() {
        System.out.println("Test get security code");
        String expResult = ("532");
        assertEquals("Incorrect integer stored", expResult, testCustomer.getSecurityCode());
    }

    @Test
    public void testSetSecurityCode() {
        System.out.println("Test get security code");
        String expResult = ("604");
        testCustomer.setSecurityCode(expResult);
        assertEquals("Incorrect integer stored", expResult, testCustomer.getSecurityCode());
    }
    
    @Test
    public void testSetEmptySecurityCode() {
        System.out.println("Set empty SecurityCode");
        String newSecurityCode = "";
        try {
            testCustomer.setSecurityCode(newSecurityCode);
            Assert.fail("Setting a empty string for SecurityCode did not throw an Illegal argument exception");
        } catch (IllegalArgumentException ex) {}
    }
    
    @Test
    public void testSetNullSecurityCode() {
        System.out.println("Set null SecurityCode");
        String newSecurityCode = null;
        try {
            testCustomer.setSecurityCode(newSecurityCode);
            Assert.fail("Setting a NULL value for SecurityCode did not throw an Illegal argument exception");
        } catch (NullPointerException ex) {}
    }
    
}
