/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AdministrationApplication;

import static org.hamcrest.CoreMatchers.equalTo;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jacob
 */
public class TierTest {
    private Tier testTier;
    private Price price;
    
    public TierTest() {
    }
    
    @Before
    public void setUp() {
        testTier = new Tier("Tier A", true, 90, 9, 10);
        price = new Price(1, 1, 1, 40.0);
        testTier.setTierTicketCost(price);
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetTierId() {
        System.out.println("Test get tier ID");
        testTier.setTierId(01);
        int expResult = (01);
        assertEquals("Incorrect integer stored", expResult, testTier.getTierId());
    }

    @Test
    public void testSetTierId() {
        System.out.println("Test set tier ID");
        int expResult = (02);
        testTier.setTierId(expResult);
        assertEquals("Incorrect integer stored", expResult, testTier.getTierId());
    }
    
    @Test
    public void testSetNegativeTierId() {
        System.out.println("Test set negative tierId");
        int newTierId = (-4);
        try {
            testTier.setTierId(newTierId);
            Assert.fail("Setting a negative tierId did not throw an IllegalArgumentException");
        } catch (IllegalArgumentException ex) {}
    }

    @Test
    public void testGetVenueId() {
        System.out.println("Test get venue ID");
        int expResult = (0);
        assertEquals("Incorrect integer stored", expResult, testTier.getVenueId());
    }
     
    @Test
    public void testSetVenueId() {
        System.out.println("Test set venue ID");
        int expResult = (04);
        testTier.setVenueId(expResult);
        assertEquals("Incorrect integer stored", expResult, testTier.getVenueId());
    }
    
    @Test
    public void testSetNegativeVenueId() {
        System.out.println("Test set negative venueId");
        int newVenue = (-4);
        try {
            testTier.setVenueId(newVenue);
            Assert.fail("Setting a negative venueId did not throw an IllegalArgumentException");
        } catch (IllegalArgumentException ex) {}
    }

    @Test
    public void testGetIsSeated() {
        System.out.println("Test get is seated");
        boolean expResult = (true);
        assertEquals("Incorrect boolean stored", expResult, testTier.getIsSeated());
    }

    @Test
    public void testSetIsSeated() {
        System.out.println("Test set is seated");
        boolean expResult = (false);
        testTier.setIsSeated(expResult);
        assertEquals("Incorrect boolean stored", expResult, testTier.getIsSeated());
    }

    @Test
    public void testGetCapacity() {
        System.out.println("Test get capacity");
        int expResult = (90);
        assertEquals("Incorrect integer stored", expResult, testTier.getCapacity());
    }

    @Test
    public void testSetCapacity() {
        System.out.println("Test set capacity");
        int expResult = (110);
        testTier.setCapacity(expResult);
        assertEquals("Incorrect integer stored", expResult, testTier.getCapacity());
    }
    
    @Test
    public void testSetNegativeCapacity() {
        System.out.println("Test set negative capacity");
        int newCapacity = (-4);
        try {
            testTier.setCapacity(newCapacity);
            Assert.fail("Setting a negative capacity did not throw an IllegalArgumentException");
        } catch (IllegalArgumentException ex) {}
    }

    @Test
    public void testGetSeatRows() {
        System.out.println("Test get seat rows");
        int expResult = (9);
        assertEquals("Incorrect integer stored", expResult, testTier.getSeatRows());
    }

    @Test
    public void testSetSeatRows() {
        System.out.println("Test set seat rows");
        int expResult = (11);
        testTier.setSeatRows(expResult);
        assertEquals("Incorrect integer stored", expResult, testTier.getSeatRows());
    }
    
    @Test
    public void testSetNegativeSeatRows() {
        System.out.println("Test set negative seat rows");
        int newSeatRows = (-4);
        try {
            testTier.setSeatRows(newSeatRows);
            Assert.fail("Setting a negative number of seat rows did not throw an IllegalArgumentException");
        } catch (IllegalArgumentException ex) {}
    }
    
    @Test
    public void testGetSeatColumns() {
        System.out.println("Test get seat columns");
        int expResult = (10);
        assertEquals("Incorrect integer stored", expResult, testTier.getSeatColumns());
    }

    @Test
    public void testSetSeatColumns() {
        System.out.println("Test set seat columns");
        int expResult = (8);
        testTier.setSeatColumns(expResult);
        assertEquals("Incorrect integer stored", expResult, testTier.getSeatColumns());
    }
    
    @Test
    public void testSetNegativeSeatColumns() {
        System.out.println("Test set negative seat columns");
        int newSeatColumns = (-4);
        try {
            testTier.setSeatColumns(newSeatColumns);
            Assert.fail("Setting a negative number of seat columns did not throw an IllegalArgumentException");
        } catch (IllegalArgumentException ex) {}
    }

    @Test
    public void testGetTierName() {
        System.out.println("Test get tier name");
        String expResult = ("Tier A");
        assertEquals("Incorrect string stored", expResult, testTier.getTierName());
    }

    @Test
    public void testSetTierName() {
        System.out.println("Test set tier name");
        String expResult = ("Tier B");
        testTier.setTierName(expResult);
        assertEquals("Incorrect string stored", expResult, testTier.getTierName());
    }
    
     @Test
    public void testSetEmptyTierName() {
        System.out.println("Test set empty tierName");
        String newTierName = "";
        try {
            testTier.setTierName(newTierName);
            Assert.fail("Setting a null tier name name did not throw a IllegalArgumentException");
        } catch (IllegalArgumentException ex) {}
    }
    
     @Test
    public void testSetNullTierName() {
        System.out.println("Test set null tierName");
        String newTierName = null;
        try {
            testTier.setTierName(newTierName);
            Assert.fail("Setting a null tier name name did not throw a NullPointerException");
        } catch (NullPointerException ex) {}
        
    }

    @Test
    public void testSetNullTicketCost() {
        System.out.println("Test set null ticketCost");
        Price newTicketCost = null;
        try {
            testTier.setTierTicketCost(newTicketCost);
            Assert.fail("Setting a null ticket cost name did not throw a NullPointerException");
        } catch (NullPointerException ex) {}
        
    }
    
}
