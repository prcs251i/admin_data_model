/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AdministrationApplication;

import static org.hamcrest.CoreMatchers.equalTo;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jacob
 */
public class PriceTest {
    private Price testPrice;
            
    public PriceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        testPrice = new Price(02,03,05, 00.00);
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetId() {
        System.out.println("Test get Id");
        int expResult = (02);
        assertEquals("Incorrect integer stored", expResult, testPrice.getId());
    }

    @Test
    public void testSetId() {
        System.out.println("Test set Id");
        int expResult = (06);
        testPrice.setId(expResult);
        assertEquals("Incorrect integer stored", expResult, testPrice.getId());
    }
    
    @Test
    public void testSetNegativeId() {
    }

    @Test
    public void testGetRunID() {
        System.out.println("Test get run Id");
        int expResult = (03);
        assertEquals("Incorrect integer stored", expResult, testPrice.getRunID());
    }

    @Test
    public void testSetRunID() {
        System.out.println("Test set run Id");
        int expResult = (04);
        testPrice.setRunID(expResult);
        assertEquals("Incorrect integer stored", expResult, testPrice.getRunID());
    }
    
    @Test
    public void testSetNegativeRunId() {
        System.out.println("Test set negative Id");
        int newId = (-3);
        try {
            testPrice.setId(newId);
            Assert.fail("Setting a negative id did not throw a IllegalArgumentException");
        } catch (IllegalArgumentException ex) {}
    }

    @Test
    public void testGetTierID() {
        System.out.println("Test get tier Id");
        int expResult = (05);
        assertEquals("Incorrect integer stored", expResult, testPrice.getTierID());
    }

    @Test
    public void testSetTierID() {
        System.out.println("Test set tier Id");
        int expResult = (07);
        testPrice.setTierID(expResult);
        assertEquals("Incorrect integer stored", expResult, testPrice.getTierID());
    }
    
    @Test
    public void testSetNegativeTierId() {
        System.out.println("Test set negative tier Id");
        int newId = (-3);
        try {
            testPrice.setTierID(newId);
            Assert.fail("Setting a negative tier id did not throw a IllegalArgumentException");
        } catch (IllegalArgumentException ex) {}
    }

    @Test
    public void testGetPrice() {
        System.out.println("Test get price");
        double expResult = (00.00);
        assertThat("Incorrect double stored", expResult, equalTo(testPrice.getPrice()));
    }

    @Test
    public void testSetPrice() {
        System.out.println("Test set prive");
        double expResult = (40.00);
        testPrice.setPrice(expResult);
        assertThat("Incorrect double stored", expResult, equalTo(testPrice.getPrice()));
    }
    
    @Test
    public void testSetNegativePrice() {
        System.out.println("Test set negative price");
        int newPrice = (-3);
        try {
            testPrice.setPrice(newPrice);
            Assert.fail("Setting a negative price did not throw a IllegalArgumentException");
        } catch (IllegalArgumentException ex) {}
    }
    
}
