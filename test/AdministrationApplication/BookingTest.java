/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AdministrationApplication;

import AdministrationApplication.*;
import java.util.ArrayList;
import java.util.Date;
import junit.framework.Assert;
import static org.hamcrest.CoreMatchers.equalTo;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jacob
 */
public class BookingTest {
    private Booking testBooking;
    private EventRun testRun, testRun2;
    private Customer testCustomer, testCustomer2;
    private Date bookingDate, testBookingDate;
    private ArrayList<Ticket> ticketList = new ArrayList();
    private ArrayList<Act> performingActsList = new ArrayList();
    private Venue testVenue;
    private Tier testTier;
    private Date expiryDate, run_Date;
    private Address testAddress;
    private SeatedTicket testTicket;
    
    public BookingTest() {
    }
    
    @Before
    public void setUp() {
        SeatedTicket testTicket;
        SeatedTicket testTicket2;
        ticketList = new ArrayList();
        
        testRun = new EventRun(02, testVenue, performingActsList, run_Date, 150);
        
        testRun2 = new EventRun(05, testVenue, performingActsList, run_Date, 132);
        
        testCustomer = new Customer("Steve", "Stevenson", "SS@gmail.com", "678 321", 
            "07450739568", testAddress, "visa", "3256 8764 2345 2318" ,expiryDate, "532", "password");
        
        testCustomer2 = new Customer("Jim", "Stevenson", "JS@gmail.com", "678 321", 
            "07450739568", testAddress, "visa", "3256 8764 2345 2318" ,expiryDate, "532", "password");
         
        testBooking = new Booking(testRun, testCustomer, bookingDate, 2, 38.00);
        
        testTicket = new SeatedTicket(02, 03, testVenue, testTier, "Row A", 32);
        ticketList.add(testTicket);
        
        testTicket2 = new SeatedTicket(03, 04, testVenue, testTier, "Row A", 33);
        ticketList.add(testTicket2);
        
        testBooking.setTicketList(ticketList);
    }
    
    @Test
    public void testGetBookingID() {
        System.out.println("Test get Booking ID");
        testBooking.setBookingID(03);
        int expResult = (03);
        assertEquals("Incorrect integer stored", expResult, testBooking.getBookingID());
    }

    @Test
    public void testSetBookingID() {
        System.out.println("Test set Booking ID");
        testBooking.setBookingID(02);
        int expResult = (02);
        assertEquals("Incorrect integer stored", expResult, testBooking.getBookingID());
    }
    
    @Test
    public void testSetNegativeBookingID() {
        System.out.println("set NEGATIVE Booking ID");
        int newID = -47600004;
        try {
            testBooking.setBookingID(newID);
            Assert.fail("Setting a negative Booking ID did not throw a Illegal argument exception");
        } catch (IllegalArgumentException ex) {} 
    }

    @Test
    public void testGetCustomer() {
        System.out.println("Test get customer");
        Customer expResult = (testCustomer);
        assertEquals("Incorrect customer stored", expResult, testBooking.getCustomer());
    }

    @Test
    public void testSetCustomer() {
        System.out.println("Test set customer");
        Customer expResult = (testCustomer2);
        testBooking.setCustomer(expResult);
        assertEquals("Incorrect customer stored", expResult, testBooking.getCustomer());
    }
    
    @Test
    public void testSetNullActName() {
        System.out.println("set NULL Customer");
        Customer newCustomer = null;
        try {
            testBooking.setCustomer(newCustomer);
            Assert.fail("Setting a NULL value for customer did not throw an Illegal argument exception");
        } catch (NullPointerException ex) {}
    }

    @Test
    public void testGetRun() {
        System.out.println("Test get run");
        EventRun expResult = (testRun);
        assertEquals("Incorrect run stored", expResult, testBooking.getRun());
    }

    @Test
    public void testSetRun() {
        System.out.println("Test set run");
        EventRun expResult = (testRun2);
        testBooking.setRun(expResult);
        assertEquals("Incorrect run stored", expResult, testBooking.getRun());
    }
    
    @Test
    public void testSetNullRun() {
        System.out.println("set NULL run");
        EventRun newRun = null;
        try {
            testBooking.setRun(newRun);
            Assert.fail("Setting a NULL value for run did not throw an Illegal argument exception");
        } catch (NullPointerException ex) {}
    }

    @Test
    public void testGetDatePlaced() {
        System.out.println("Test get date placed");
        Date expResult = (bookingDate);
        assertEquals("Incorrect date stored", expResult, testBooking.getDatePlaced());
    }

    @Test
    public void testSetDatePlaced() {
        System.out.println("Test set date placed");
        Date expResult = (testBookingDate);
        testBooking.setDatePlaced(expResult);
        assertEquals("Incorrect date stored", expResult, testBooking.getDatePlaced());
    }

    @Test
    public void testGetNoOfTickets() {
        System.out.println("Test get no of tickets");
        int expResult = (2);
        assertEquals("Incorrect no of tickets stored", expResult, testBooking.getNoOfTickets());
    }

    @Test
    public void testSetNoOfTickets() {
        System.out.println("Test set no of tickets");
        int expResult = (4);
        testBooking.setNoOfTickets(expResult);
        assertEquals("Incorrect no of tickets stored", expResult, testBooking.getNoOfTickets());
    }
    
    @Test
    public void testSetNegativeNoOfTickets() {
        System.out.println("set NEGATIVE no of tickets");
        int newNoOfTickets = -4;
        try {
            testBooking.setNoOfTickets(newNoOfTickets);
            Assert.fail("Setting a negative number of tickets did not throw a Illegal argument exception");
        } catch (IllegalArgumentException ex) {} 
    }

    @Test
    public void testGetTicketList() {
        System.out.println("Test get ticket list");
        boolean value = false;
        for(Ticket objCurrTicket : testBooking.getTicketList()) {
            if(objCurrTicket.getTicketRef() == (03)) {
                value = true;
            }
        }
        assertTrue(value);
    }

    @Test
    public void testSetTicketList() {
        System.out.println("Test set ticket list");
        boolean value = false;     
        testBooking.setTicketList(ticketList);
        for(Ticket objCurrTicket : testBooking.getTicketList()) {
            if(objCurrTicket.getTicketRef() == (02) || objCurrTicket.getTicketRef() == (03)) {
                value = true;
            }
        }
        assertTrue(value);
    }

    @Test
    public void testGetTotalCost() {
        System.out.println("Test get total cost");
        double expResult = (38.00);
        assertThat("Incorrect cost stored",expResult, equalTo(testBooking.getTotalCost()));
    }

    @Test
    public void testSetTotalCost() {
        System.out.println("Test set total cost");
        double expResult = (45.00);
        testBooking.setTotalCost(expResult);
        assertThat("Incorrect cost stored",expResult, equalTo(testBooking.getTotalCost()));
    }
    
    @Test
    public void testSetNegativeCost() {
        System.out.println("set NEGATIVE cost");
        int newCost = -4;
        try {
            testBooking.setTotalCost(newCost);
            Assert.fail("Setting a negative total cost did not throw a Illegal argument exception");
        } catch (IllegalArgumentException ex) {} 
    }
    
}
