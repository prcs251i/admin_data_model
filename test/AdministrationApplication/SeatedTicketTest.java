/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AdministrationApplication;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jacob
 */
public class SeatedTicketTest {
    private Venue testVenue;
    private Tier testTier;
    private SeatedTicket testTicket;
    
    public SeatedTicketTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        testTicket = new SeatedTicket(02, 03, testVenue, testTier, "Row A", 32);
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetSeatNo() {
        System.out.println("Test get seat No");
        int expResult = (32);
        assertEquals("Incorrect integer stored", expResult, testTicket.getSeatNo());
    }

    @Test
    public void testSetSeatNo() {
        System.out.println("Test set seat No");
        int expResult = (42);
        testTicket.setSeatNo(expResult);
        assertEquals("Incorrect integer stored", expResult, testTicket.getSeatNo());
    }
    
    @Test
    public void testSetNegativeSeatNo() {
        System.out.println("Test set empty HomeNumber");
        int newSeatNo = (-3);
        try {
            testTicket.setSeatNo(newSeatNo);
            Assert.fail("Setting a empty string for SeatNo did not throw an Illegal argument exception");
        } catch (IllegalArgumentException ex) {}
    }

    @Test
    public void testGetSeatRow() {
        System.out.println("Test get seat Row");
        String expResult = ("Row A");
        assertEquals("Incorrect integer stored", expResult, testTicket.getSeatRow());
    }

    @Test
    public void testSetSeatRow() {
        System.out.println("Test set seat Row");
        String expResult = ("Row B");
        testTicket.setSeatRow(expResult);
        assertEquals("Incorrect integer stored", expResult, testTicket.getSeatRow());
    }
    
    @Test
    public void testSetEmptySeatRow() {
        System.out.println("Test set empty SeatRow");
        String newSeatRow = "";
        try {
            testTicket.setSeatRow(newSeatRow);
            Assert.fail("Setting a empty string for seat row did not throw an Illegal argument exception");
        } catch (IllegalArgumentException ex) {}
    }
    
    @Test
    public void testSetNullSeatRow() {
        System.out.println("Test set null SeatRow");
        String newSeatRow = null;
        try {
            testTicket.setSeatRow(newSeatRow);
            Assert.fail("Setting a null value for SeatRow did not throw an Null pointer exception");
        } catch (NullPointerException ex) {}
    }
    
}
