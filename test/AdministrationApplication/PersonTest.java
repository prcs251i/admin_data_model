/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AdministrationApplication;

import AdministrationApplication.Person;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jacob
 */
public class PersonTest {
    private Person testPerson;
    
    public PersonTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        testPerson = new Person("Jack", "Smith", "jSmith@gmail.com", "password") {};
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetID() {
        System.out.println("Test get Id");
        testPerson.setID(05);
        int expResult = (05);
        assertEquals("incorrect integer stored", expResult, testPerson.getID());
    }

    @Test
    public void testSetID() {
        System.out.println("Test set Id");
        int expResult = (02);
        testPerson.setID(expResult);
        assertEquals("incorrect integer stored", expResult, testPerson.getID());
    }
    
    @Test
    public void testSetNegativeID() {
        System.out.println("Test set negative Id");
        int newId = (-3);
        try {
            testPerson.setID(newId);
            Assert.fail("Setting a negative id did not throw a IllegalArgumentException");
        } catch (IllegalArgumentException ex) {}
    }

    @Test
    public void testGetForename() {
        System.out.println("Test get forename");
        String expResult = ("Jack");
        assertEquals("Incorrect string stored", expResult, testPerson.getForename());
    }

    @Test
    public void testSetForename() {
        System.out.println("Test set forename");
        String expResult = ("Jill");
        testPerson.setForename(expResult);
        assertEquals("Incorrect string stored", expResult, testPerson.getForename());
    }
    
    @Test
    public void testSetEmptyForename() {
        System.out.println("Test set empty forename");
        String newForename = ("");
        try {
            testPerson.setForename(newForename);
            Assert.fail("Setting a empty forename did not throw a IllegalArgumentException");
        } catch (IllegalArgumentException ex) {}
    }

    @Test
    public void testSetNullForename() { 
        System.out.println("Test set null forename");
        String newForename = null;
        try {
            testPerson.setForename(newForename);
            Assert.fail("Setting a null forename did not throw a NullpointerException");
        } catch (NullPointerException ex) {}
    }
    
    @Test
    public void testGetSurname() {
        System.out.println("Test get surname");
        String expResult = ("Smith");
        assertEquals("Incorrect string stored", expResult, testPerson.getSurname());
    }

    @Test
    public void testSetSurname() {
        System.out.println("Test set surname");
        String expResult = ("Jones");
        testPerson.setSurname(expResult);
        assertEquals("Incorrect string stored", expResult, testPerson.getSurname());
    }
    
    @Test
    public void testSetEmptySurname() { 
        System.out.println("Test set empty surname");
        String newSurname = ("");
        try {
            testPerson.setSurname(newSurname);
            Assert.fail("Setting a empty surname did not throw a IllegalArgumentException");
        } catch (IllegalArgumentException ex) {}
    }

    @Test
    public void testSetNullSurname() { 
        System.out.println("Test set null surname");
        String newSurname = null;
        try {
            testPerson.setForename(newSurname);
            Assert.fail("Setting a null surname did not throw a NullpointerException");
        } catch (NullPointerException ex) {}
    }

    @Test
    public void testGetEmail() {
        System.out.println("Test get email");
        String expResult = ("jSmith@gmail.com");
        assertEquals("Incorrect string stored", expResult, testPerson.getEmail());
    }

    @Test
    public void testSetEmail() {
        System.out.println("Test set email");
        String expResult = ("jJones@gmail.com");
        testPerson.setEmail(expResult);
        assertEquals("Incorrect string stored", expResult, testPerson.getEmail());
    }
    
    @Test
    public void testSetEmptyEmail() {  
        System.out.println("Test set empty email");
        String newEmail = ("");
        try {
            testPerson.setEmail(newEmail);
            Assert.fail("Setting a empty email did not throw a IllegalArgumentException");
        } catch (IllegalArgumentException ex) {}
    }

    @Test
    public void testSetNullEmail() {
        System.out.println("Test set null email");
        String newEmail = null;
        try {
            testPerson.setEmail(newEmail);
            Assert.fail("Setting a null email did not throw a NullpointerException");
        } catch (NullPointerException ex) {}
    }
    
    @Test
    public void testGetPassword() {
        System.out.println("Test get password");
        String expResult = ("password");
        assertEquals("Incorrect string stored", expResult, testPerson.getPassword());
    }

    @Test
    public void testSetPassword() {
        System.out.println("Test set password");
        String expResult = ("newpassword");
        testPerson.setPassword(expResult);
        assertEquals("Incorrect string stored", expResult, testPerson.getPassword());
    }
    
    @Test
    public void testSetEmptyPassword() {  
        System.out.println("Test set empty password");
        String newPassword = ("");
        try {
            testPerson.setPassword(newPassword);
            Assert.fail("Setting a empty password did not throw a IllegalArgumentException");
        } catch (IllegalArgumentException ex) {}
    }

    @Test
    public void testSetNullPassword() {   
    }

    public class PersonImpl extends Person {
    }
    
}
