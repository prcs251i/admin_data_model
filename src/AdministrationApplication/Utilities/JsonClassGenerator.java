package AdministrationApplication.Utilities;

import AdministrationApplication.Act;
import AdministrationApplication.Address;
import AdministrationApplication.Administrator;
import AdministrationApplication.Booking;
import AdministrationApplication.Customer;
import AdministrationApplication.Event;
import AdministrationApplication.EventRun;
import AdministrationApplication.EventType;
import AdministrationApplication.GuestBooking;
import AdministrationApplication.Price;
import AdministrationApplication.Review;
import AdministrationApplication.SeatedTicket;
import AdministrationApplication.StandingTicket;
import AdministrationApplication.Ticket;
import AdministrationApplication.Tier;
import AdministrationApplication.Venue;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Joel Gooch
 */
public class JsonClassGenerator {

    private URL api;
    private String json;

    /**
     * default constructor
     */
    public JsonClassGenerator() {
        api = null;
        json = "";
    }

    /**
     *
     * @param finalObject - JSONObject containing single Act data
     * @return Act object constructed from JSONObject
     * @throws JSONException
     */
    private Act generateAct(JSONObject finalObject) {
        Act act = new Act();
        act.setID(finalObject.getInt("ID"));
        act.setActName(finalObject.getString("TITLE"));
        try {
            act.setActDesc(finalObject.getString("DESCRIPTION"));
        } catch (JSONException ex) {
        }
        JSONObject actType = finalObject.getJSONObject("TYPE");
        EventType type = generateEventType(actType);
        act.setActType(type);
        return act;
    }

    /**
     *
     * @return list of all Act objects in database
     * @throws MalformedURLException
     * @throws IOException
     * @throws JSONException
     */
    public List<Act> generateActs() throws MalformedURLException, IOException, JSONException {
        api = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/acts");
        json = IOUtils.toString(api);
        JSONArray acts = new JSONArray(json);
        List<Act> actList = new ArrayList();
        for (int i = 0; i < acts.length(); i++) {
            JSONObject finalObject = acts.getJSONObject(i);
            Act act = generateAct(finalObject);
            actList.add(act);
            actList.sort(act);
        }
        return actList;
    }

    /**
     *
     * @param finalObject - JSONObject containing single address data
     * @return Address object constructed from JSONObject
     * @throws JSONException
     */
    private Address generateAddress(JSONObject finalObject) throws JSONException {
        Address venueAddress = new Address();
        venueAddress.setAddressLine1(finalObject.getString("ADDRESS_LINE_1"));
        try {
            venueAddress.setAddressLine2(finalObject.getString("ADDRESS_LINE_2"));
        } catch (JSONException ex) {
        }
        venueAddress.setCity(finalObject.getString("CITY"));
        venueAddress.setCounty(finalObject.getString("COUNTY"));
        venueAddress.setPostcode(finalObject.getString("POSTCODE"));
        return venueAddress;
    }

    /**
     *
     * @return list of all Administrator objects in database
     * @throws MalformedURLException
     * @throws IOException
     * @throws JSONException
     */
    public List<Administrator> generateAdministrators() throws MalformedURLException, IOException, JSONException {
        api = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/admins");
        json = IOUtils.toString(api);
        JSONArray admins = new JSONArray(json);
        List<Administrator> adminList = new ArrayList();
        for (int i = 0; i < admins.length(); i++) {
            JSONObject finalObject = admins.getJSONObject(i);
            Administrator admin = generateAdmin(finalObject);
            adminList.add(admin);
            adminList.sort(admin);
        }
        return adminList;
    }

    /**
     *
     * @param finalObject - JSONObject containing single Administrator data
     * @return Administrator object constructed from JSONObject
     * @throws JSONException
     */
    private Administrator generateAdmin(JSONObject finalObject) throws JSONException {
        Administrator admin = new Administrator();
        admin.setID(finalObject.getInt("ID"));
        admin.setForename(finalObject.getString("FIRST_NAME"));
        admin.setSurname(finalObject.getString("LAST_NAME"));
        admin.setEmail(finalObject.getString("EMAIL"));
        admin.setPassword(finalObject.getString("PASSWORD_HASH"));
        return admin;
    }

    /**
     *
     * @return list of all Booking objects in database
     * @throws MalformedURLException
     * @throws IOException
     * @throws JSONException
     */
    public List<Booking> generateBookings() throws MalformedURLException, IOException, JSONException {
        api = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/bookings");
        json = IOUtils.toString(api);
        JSONArray bookings = new JSONArray(json);
        List<Booking> bookingList = new ArrayList();
        for (int i = 0; i < bookings.length(); i++) {
            JSONObject finalObject = bookings.getJSONObject(i);
            Booking booking = generateBooking(finalObject);
            bookingList.add(booking);
            bookingList.sort(booking);
        }
        return bookingList;
    }

    /**
     *
     * @param finalObject - JSONObject containing single Booking data
     * @return Booking object constructed from JSONObject
     * @throws JSONException
     */
    private Booking generateBooking(JSONObject finalObject) throws JSONException {
        Booking booking = new Booking();
        booking.setBookingID(finalObject.getInt("ID"));
        JSONObject eventObject = finalObject.getJSONObject("RUN");
        EventRun ticketRun = generateRun(eventObject);
        booking.setRun(ticketRun);
        JSONObject customerObject = finalObject.getJSONObject("CUSTOMER");
        Customer customerBooked = generateCustomer(customerObject);
        booking.setCustomer(customerBooked);
        String dateString = finalObject.getString("ORDER_DATE");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        try {
            Date orderDate = df.parse(dateString);
            booking.setDatePlaced(orderDate);
        } catch (ParseException e) {
        }
        booking.setNoOfTickets(finalObject.getInt("QUANTITY"));
        List<Ticket> ticketList = new ArrayList();
        for (int j = 0; j < finalObject.getJSONArray("TICKETS").length(); j++) {
            JSONObject ticketsObject = finalObject.getJSONArray("TICKETS").getJSONObject(j);
            JSONObject tierObject = ticketsObject.getJSONObject("TIER");
            Tier ticketTier = generateTier(tierObject);
            if (ticketTier.getIsSeated() == true) {
                SeatedTicket ticket = generateSeatedTicket(ticketsObject, ticketTier);
                ticketList.add(ticket);
                ticketList.sort(ticket);
            } else if (ticketTier.getIsSeated() == false) {
                StandingTicket ticket = generateStandingTicket(ticketsObject, ticketTier);
                ticketList.add(ticket);
                ticketList.sort(ticket);
            }
        }
        booking.setTicketList(ticketList);
        booking.setTotalCost(finalObject.getDouble("TOTAL_COST"));
        return booking;
    }

    /**
     *
     * @param finalObject - JSONObject containing single Customer data
     * @return Customer object constructed from JSONObject
     * @throws JSONException
     */
    private Customer generateCustomer(JSONObject finalObject) throws JSONException {
        Customer customer = new Customer();
        customer.setID(finalObject.getInt("ID"));
        customer.setSurname(finalObject.getString("SURNAME"));
        customer.setForename(finalObject.getString("FORENAME"));
        customer.setEmail(finalObject.getString("EMAIL"));
        try {
            customer.setHomeNumber(finalObject.getString("HOME_PHONE"));
            customer.setMobNumber(finalObject.getString("MOB_PHONE"));
        } catch (JSONException ex) {
        }
        Address address = generateAddress(finalObject);
        customer.setAddress(address);
        customer.setCardType(finalObject.getString("CARD_TYPE"));
        customer.setCardNumber(finalObject.getString("CARD_NUMBER"));
        String dateString = finalObject.getString("EXPIRY_DATE");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        try {
            Date expDate = df.parse(dateString);
            customer.setExpiryDate(expDate);
        } catch (ParseException e) {
        }
        customer.setSecurityCode(finalObject.getString("SECURITY_CODE"));
        customer.setPassword(finalObject.getString("PASSWORD_HASH"));
        return customer;
    }

    /**
     *
     * @return list of all Customer objects in database
     * @throws MalformedURLException
     * @throws IOException
     * @throws JSONException
     */
    public List<Customer> generateCustomers() throws MalformedURLException, IOException, JSONException {
        api = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/customers");
        json = IOUtils.toString(api);
        JSONArray customers = new JSONArray(json);
        List<Customer> customerList = new ArrayList();
        for (int i = 0; i < customers.length(); i++) {
            JSONObject finalObject = customers.getJSONObject(i);
            Customer customer = generateCustomer(finalObject);
            customerList.add(customer);
            customerList.sort(customer);
        }
        return customerList;
    }

    /**
     *
     * @return list of all GuestBooking objects in database
     * @throws MalformedURLException
     * @throws IOException
     */
    public List<GuestBooking> generateGuestBookings() throws MalformedURLException, IOException {
        api = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/guest_bookings");
        json = IOUtils.toString(api);
        JSONArray bookings = new JSONArray(json);
        List<GuestBooking> bookingList = new ArrayList();
        for (int i = 0; i < bookings.length(); i++) {
            JSONObject finalObject = bookings.getJSONObject(i);
            GuestBooking booking = generateGuestBooking(finalObject);
            bookingList.add(booking);
            bookingList.sort(booking);
        }
        return bookingList;
    }

    /**
     *
     * @param finalObject - JSONObject containing single GuestBooking data
     * @return GuestBooking object constructed from JSONObject
     * @throws JSONException
     */
    GuestBooking generateGuestBooking(JSONObject finalObject) {
        GuestBooking booking = new GuestBooking();
        booking.setBookingID(finalObject.getInt("ID"));
        JSONObject eventObject = finalObject.getJSONObject("RUN");
        EventRun ticketRun = generateRun(eventObject);
        booking.setRun(ticketRun);
        booking.setFirstName(finalObject.getString("FORENAME"));
        booking.setLastName(finalObject.getString("SURNAME"));
        booking.setEmail(finalObject.getString("EMAIL"));
        String dateString = finalObject.getString("ORDER_DATE");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        try {
            Date orderDate = df.parse(dateString);
            booking.setDatePlaced(orderDate);
        } catch (ParseException e) {

        }
        booking.setNoOfTickets(finalObject.getInt("QUANTITY"));
        List<Ticket> ticketList = new ArrayList();
        for (int j = 0; j < finalObject.getJSONArray("TICKETs").length(); j++) {
            JSONObject ticketsObject = finalObject.getJSONArray("TICKETs").getJSONObject(j);
            JSONObject tierObject = ticketsObject.getJSONObject("TIER");
            Tier ticketTier = generateTier(tierObject);
            if (ticketTier.getIsSeated() == true) {
                SeatedTicket ticket = generateSeatedTicket(ticketsObject, ticketTier);
                ticketList.add(ticket);
                ticketList.sort(ticket);
            } else if (ticketTier.getIsSeated() == false) {
                StandingTicket ticket = generateStandingTicket(ticketsObject, ticketTier);
                ticketList.add(ticket);
                ticketList.sort(ticket);
            }
        }
        booking.setTicketList(ticketList);
        booking.setTotalCost(finalObject.getDouble("TOTAL_COST"));
        return booking;
    }

    /**
     *
     * @param finalObject - JSONObject containing single event data
     * @return Event object constructed from JSONObject
     * @throws JSONException
     */
    private Event generateEvent(JSONObject finalObject) throws JSONException {
        Event event = new Event();
        event.setID(finalObject.getInt("ID"));
        List<Review> reviewsList = new ArrayList();
        for (int j = 0; j < finalObject.getJSONArray("REVIEWS").length(); j++) {
            JSONObject reviewsObject = finalObject.getJSONArray("REVIEWS").getJSONObject(j);
            Review review = generateReview(reviewsObject);
            reviewsList.add(review);
            reviewsList.sort(review);
        }
        event.setReviewsList(reviewsList);
        JSONObject eventTypeObject = finalObject.getJSONObject("TYPE");
        EventType type = generateEventType(eventTypeObject);
        event.setEventType(type);
        event.setEventTitle(finalObject.getString("TITLE"));
        List<EventRun> runList = new ArrayList();
        for (int k = 0; k < finalObject.getJSONArray("RUNS").length(); k++) {
            JSONObject runObject = finalObject.getJSONArray("RUNS").getJSONObject(k);
            EventRun run = generateRun(runObject);
            runList.add(run);
            runList.sort(run);
        }
        event.setEventRuns(runList);
        event.setAgeRating(finalObject.getString("AGE_RATING"));
        event.setAverageRating(event.calculateAverageRating());
        event.setEventDescription(finalObject.getString("DESCRIPTION"));
        event.setImageString(finalObject.getString("IMAGE"));
        return event;
    }

    /**
     *
     * @return list of all Event objects in database
     * @throws MalformedURLException
     * @throws IOException
     * @throws JSONException
     */
    public List<Event> generateEvents() throws MalformedURLException, IOException, JSONException {
        api = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/events");
        json = IOUtils.toString(api);
        JSONArray events = new JSONArray(json);
        List<Event> eventList = new ArrayList();
        for (int i = 0; i < events.length(); i++) {
            JSONObject finalObject = events.getJSONObject(i);
            Event event = generateEvent(finalObject);
            eventList.add(event);
            eventList.sort(event);
        }
        return eventList;
    }

    /**
     *
     * @return list of all EventType objects in database
     * @throws MalformedURLException
     * @throws IOException
     * @throws JSONException
     */
    public List<EventType> generateEventTypes() throws MalformedURLException, IOException, JSONException {
        api = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/event_types");
        json = IOUtils.toString(api);
        JSONArray eventTypes = new JSONArray(json);
        List<EventType> eventTypesList = new ArrayList();
        for (int i = 0; i < eventTypes.length(); i++) {
            JSONObject finalObject = eventTypes.getJSONObject(i);
            EventType type = generateEventType(finalObject);
            eventTypesList.add(type);
            eventTypesList.sort(type);
        }

        return eventTypesList;
    }

    /**
     *
     * @param finalObject - JSONObject containing single EventType data
     * @return EventType object constructed from JSONObject
     * @throws JSONException
     */
    private EventType generateEventType(JSONObject finalObject) throws JSONException {
        EventType type = new EventType();
        type.setID(finalObject.getInt("ID"));
        type.setType(finalObject.getString("NAME"));
        return type;
    }

    /**
     *
     * @param finalObject - JSONObject containing single Price data
     * @return Price object constructed from JSONObject
     * @throws JSONException
     */
    public Price generatePrice(JSONObject finalObject) throws JSONException {
        Price price = new Price();
        price.setId(finalObject.getInt("ID"));
        price.setRunID(finalObject.getInt("RUN_ID"));
        price.setTierID(finalObject.getInt("TIER_ID"));
        price.setPrice(finalObject.getDouble("PRICE1"));
        return price;
    }

    /**
     *
     * @param finalObject - JSONObject containing single Review data
     * @return Review object constructed from JSONObject
     * @throws JSONException
     */
    private Review generateReview(JSONObject finalObject) throws JSONException {
        Review review = new Review();
        review.setReviewID(finalObject.getInt("ID"));
        review.setEventID(finalObject.getInt("EVENT_ID"));
        review.setUserID(finalObject.getInt("USER_ID"));
        String dateString = finalObject.getString("REVIEW_DATE");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        try {
            Date reviewDate = df.parse(dateString);
            review.setReviewDate(reviewDate);
        } catch (ParseException e) {
        }
        review.setUserRating(finalObject.getInt("USER_RATING"));
        try {
            review.setUserComments(finalObject.getString("USER_COMMENTS"));
        } catch (JSONException ex) {
        }
        return review;
    }

    /**
     *
     * @return list of all Review objects in database
     * @throws MalformedURLException
     * @throws IOException
     * @throws JSONException
     */
    public List<Review> generateReviews() throws MalformedURLException, IOException, JSONException {
        api = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/reviews");
        json = IOUtils.toString(api);
        JSONArray reviews = new JSONArray(json);
        List<Review> reviewList = new ArrayList();
        for (int i = 0; i < reviews.length(); i++) {
            JSONObject finalObject = reviews.getJSONObject(i);
            Review review = generateReview(finalObject);
            reviewList.add(review);
            reviewList.sort(review);
        }
        return reviewList;
    }

    /**
     *
     * @param finalObject - JSONObject containing single Run data
     * @return Run object constructed from JSONObject
     * @throws JSONException
     */
    private EventRun generateRun(JSONObject finalObject) throws JSONException {
        EventRun run = new EventRun();
        run.setID(finalObject.getInt("ID"));
        JSONObject venue = finalObject.getJSONObject("VENUE");
        Venue eventVenue = generateVenue(venue);
        run.setVenue(eventVenue);
        List<Price> priceList = new ArrayList();
        for (int i = 0; i < finalObject.getJSONArray("PRICES").length(); i++) {
            JSONObject pricesObject = finalObject.getJSONArray("PRICES").getJSONObject(i);
            Price price = generatePrice(pricesObject);
            priceList.add(price);
            priceList.sort(price);
        }
        for (Tier tier : run.getVenue().getTierList()) {
            for (Price price : priceList) {
                if (price.getTierID() == tier.getTierId()) {
                    tier.setTierTicketCost(price);
                }
            }
        }
        List<Act> performersList = new ArrayList();
        for (int i = 0; i < finalObject.getJSONArray("PERFORMANCES").length(); i++) {
            JSONObject performersObject = finalObject.getJSONArray("PERFORMANCES").getJSONObject(i);
            JSONObject actsObject = performersObject.getJSONObject("ACT");
            Act performer = generateAct(actsObject);
            performer.setPerformanceID(performersObject.getInt("ID"));
            performersList.add(performer);
            performersList.sort(performer);
        }
        run.setActsPerforming(performersList);
        String dateString = finalObject.getString("RUN_DATE");
        DateFormat date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");;
        try {
            Date runDate = date.parse(dateString);
            run.setRunDate(runDate);
        } catch (ParseException e) {
        }
        run.setDuration(finalObject.getInt("DURATION"));
        return run;
    }

    /**
     *
     * @param finalObject - JSONObject containing single Seated Ticket data
     * @return Seated Ticket object constructed from JSONObject
     * @throws JSONException
     */
    private SeatedTicket generateSeatedTicket(JSONObject finalObject, Tier ticketTier) throws JSONException {
        SeatedTicket ticket = new SeatedTicket();
        ticket.setTicketRef(finalObject.getInt("ID"));
        try {
            ticket.setBookingID(finalObject.getInt("BOOKING_ID"));
        } catch (JSONException ex) {
        }
        ticket.setAllocatedTier(ticketTier);
        ticket.setSeatRow(finalObject.getString("SEAT_ROW"));
        ticket.setSeatNo(finalObject.getInt("SEAT_NUM"));
        return ticket;
    }

    /**
     *
     * @param finalObject - JSONObject containing single standing Ticket data
     * @return Standing Ticket object constructed from JSONObject
     * @throws JSONException
     */
    private StandingTicket generateStandingTicket(JSONObject finalObject, Tier ticketTier) throws JSONException {
        StandingTicket ticket = new StandingTicket();
        ticket.setTicketRef(finalObject.getInt("ID"));
        try {
            ticket.setBookingID(finalObject.getInt("BOOKING_ID"));
        } catch (JSONException ex) {
        }
        ticket.setAllocatedTier(ticketTier);
        return ticket;
    }

    /**
     *
     * @param finalObject - JSONObject containing single Tier data
     * @return Tier object constructed from JSONObject
     * @throws JSONException
     */
    private Tier generateTier(JSONObject finalObject) throws JSONException {
        Tier tier = new Tier();
        tier.setTierId(finalObject.getInt("ID"));
        tier.setVenueId(finalObject.getInt("VENUE_ID"));
        tier.setTierName(finalObject.getString("TIER_NAME"));
        if (finalObject.getString("SEATED").charAt(0) == 'Y') {
            tier.setIsSeated(true);
        } else if (finalObject.getString("SEATED").charAt(0) == 'N') {
            tier.setIsSeated(false);
        }
        tier.setCapacity(finalObject.getInt("CAPACITY"));
        try {
            tier.setSeatRows(finalObject.getInt("SEAT_ROWS"));
            tier.setSeatColumns(finalObject.getInt("SEAT_COLUMNS"));
        } catch (JSONException ex) {
        }
        return tier;
    }

    /**
     *
     * @param finalObject - JSONObject containing single venue data
     * @return Venue object constructed from JSONObject
     * @throws JSONException
     */
    private Venue generateVenue(JSONObject finalObject) throws JSONException {
        Venue venue = new Venue();
        venue.setVenueID(finalObject.getInt("ID"));
        venue.setVenueName(finalObject.getString("NAME"));
        Address venueAddress = generateAddress(finalObject);
        venue.setVenueAddress(venueAddress);
        List<Tier> tierList = new ArrayList();
        for (int j = 0; j < finalObject.getJSONArray("TIERS").length(); j++) {
            JSONObject tiersObject = finalObject.getJSONArray("TIERS").getJSONObject(j);
            Tier tier = generateTier(tiersObject);
            tierList.add(tier);
            tierList.sort(tier);
        }
        venue.addTiers(tierList);
        return venue;
    }

    /**
     *
     * @return list of all Venue objects in database
     * @throws MalformedURLException
     * @throws IOException
     * @throws JSONException
     */
    public List<Venue> generateVenues() throws MalformedURLException, IOException, JSONException {
        api = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/venues");
        json = IOUtils.toString(api);
        JSONArray venues = new JSONArray(json);
        List<Venue> venueList = new ArrayList();
        for (int i = 0; i < venues.length(); i++) {
            JSONObject finalObject = venues.getJSONObject(i);
            Venue venue = generateVenue(finalObject);
            venueList.add(venue);
            venueList.sort(venue);
        }
        return venueList;
    }

}
