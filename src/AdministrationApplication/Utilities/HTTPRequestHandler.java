package AdministrationApplication.Utilities;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author Joel Gooch
 */
public class HTTPRequestHandler {

    private HttpURLConnection conn;
    private CookieManager cookieManager;
    private String info;

    /**
     * default constructor
     */
    public HTTPRequestHandler() {
        conn = null;
    }

    /**
     *
     * @param loginDetail - json string of username and password to be send via
     * POST request
     * @return string from middleware that contains authorization token
     * @throws IOException
     */
    public String[] authenticate(String loginDetail) throws IOException {
        URL authenticateURL = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/auth/adminAuthRequest");
        CookieManager cookiemanager = new CookieManager();
        cookiemanager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        CookieHandler.setDefault(cookiemanager);
        return sendPOSTRequest(loginDetail, authenticateURL);
    }

    /**
     *
     * @param data - json string to be sent via POST
     * @param url - url to send POST request to
     * @return string that contains http code and message in index [0], extra
     * information in index [1] such as allocated ticket information when making
     * a new booking
     * @throws ProtocolException
     * @throws IOException
     */
    public String[] sendPOSTRequest(String data, URL url) throws ProtocolException, IOException {
        String httpInfo[] = new String[2];
        try {
            byte[] postData = data.getBytes(StandardCharsets.UTF_8);
            int postDataLength = postData.length;
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("charset", "utf-8");
            conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
            try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
                wr.write(postData);
                wr.close();
            }
        } catch (MalformedURLException ex) {
        } catch (ProtocolException ex) {
        } catch (IOException ex) {
        }
        httpInfo[0] = conn.getResponseCode() + conn.getResponseMessage();
        httpInfo[1] = IOUtils.toString(conn.getInputStream(), "UTF-8");
        conn.disconnect();
        return httpInfo;
    }

    /**
     *
     * @param data - json string to be sent via PUT
     * @param url - url to send PUT request to
     * @return string containing http status code and message
     * @throws ProtocolException
     * @throws IOException
     */
    public String sendPUTRequest(String data, URL url) throws ProtocolException, IOException {
        try {
            byte[] putData = data.getBytes(StandardCharsets.UTF_8);
            int putDataLength = putData.length;
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("PUT");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("charset", "utf-8");
            conn.setRequestProperty("Content-Length", Integer.toString(putDataLength));
            try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
                wr.write(putData);
                wr.close();
            }
        } catch (MalformedURLException ex) {
        } catch (ProtocolException ex) {
        } catch (IOException ex) {
        }
        info = conn.getResponseCode() + conn.getResponseMessage();
        conn.disconnect();
        return info;
    }

    /**
     *
     * @param url - url to send DELETE request on
     * @return string containing http status code and message
     * @throws IOException
     */
    public String sendDELETERequest(URL url) throws IOException {
        try {
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("DELETE");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.connect();
        } catch (MalformedURLException ex) {
        } catch (ProtocolException ex) {
        } catch (IOException ex) {
        }
        info = conn.getResponseCode() + conn.getResponseMessage();
        conn.disconnect();
        return info;
    }

}
