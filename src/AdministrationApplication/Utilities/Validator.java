package AdministrationApplication.Utilities;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Joel Gooch
 */

public class Validator {

    private Matcher matcher;

    /**
     * Default constructor
     */
    public Validator() {

    }

    private String removeSpaces(String aString) {
        return aString.replaceAll(" ", "");
    }

    /**
     *
     * method for validating email address
     * 
     * @param email - email to be validated
     * @return boolean if email is valid or not
     */
    public boolean validateEmail(String email) {
        boolean validEmail = false;
        String emailNoSpaces = removeSpaces(email);
        Pattern emailPattern = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        matcher = emailPattern.matcher(emailNoSpaces);
        if (matcher.find()) {
            validEmail = true;
        }
        return validEmail;
    }

    /**
     *
     * method for validating UK postcodes
     * 
     * @param postcode - postcode to be validated
     * @return boolean if postcode was valid or not
     */
    public boolean validateUKPostcode(String postcode) {
        boolean validPostcode = false;
        String postcodeNoSpaces = removeSpaces(postcode);
        Pattern postcodePattern = Pattern.compile("^[A-Z]{1,2}[0-9R][0-9A-Z]?[0-9][ABD-HJLNP-UW-Z]{2}$", Pattern.CASE_INSENSITIVE);
        matcher = postcodePattern.matcher(postcodeNoSpaces);
        if (matcher.find()) {
            validPostcode = true;
        }
        return validPostcode;
    }

    /**
     *
     * method for validating supported card type numbers
     * 
     * @param cardNo - card number to be validated
     * @return boolean if card number was valid or not
     */
    public boolean validateCardNumber(String cardNo) {
        boolean validCardNo = false;
        String cardNoSpaces = removeSpaces(cardNo);
        try {
            long number = Long.parseLong(cardNoSpaces);
            if (cardNoSpaces.length() > 12 && cardNoSpaces.length() < 17) {
                validCardNo = true;
            } else {
                validCardNo = false;
            }
        } catch (NumberFormatException ex) {
        }
        return validCardNo;
    }

    /**
     *
     * method for validating valid payment card security number
     * 
     * @param secNo - payment card number to be validated
     * @return boolean if security number was valid or not
     */
    public boolean validateSecurityNo(String secNo) {
        boolean validSecNo = false;
        String secNoSpaces = removeSpaces(secNo);
        try {
            Integer.parseInt(secNoSpaces);
            if (secNoSpaces.length() == 3) {
                validSecNo = true;
            } else {
                validSecNo = false;
            }
        } catch (NumberFormatException ex) {
        }
        return validSecNo;
    }

    /**
     *
     * method for validating uk home phone numbers
     * 
     * @param homeNo - phone number to be validated
     * @return if phone number was valid or not
     */
    public boolean validateHomeNumber(String homeNo) {
        boolean validHomeNo = false;
        String homeNoSpaces = removeSpaces(homeNo);
        Pattern homeNoPattern = Pattern.compile("^0[0-9]*$", Pattern.CASE_INSENSITIVE);
        matcher = homeNoPattern.matcher(homeNoSpaces);
        if (matcher.find()) {
            try {
                long number = Long.parseLong(homeNoSpaces);
                if (homeNoSpaces.length() < 12 && homeNoSpaces.length() > 6) {
                    validHomeNo = true;
                } else {
                    validHomeNo = false;
                }
            } catch (NumberFormatException ex) {
            }
        } else {
            return false;
        }
        return validHomeNo;
    }
}
