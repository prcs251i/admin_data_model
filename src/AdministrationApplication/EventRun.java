package AdministrationApplication;

import AdministrationApplication.Interfaces.IEventRun;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Joel Gooch
 */

public class EventRun implements IEventRun, Comparator<EventRun> {

    private int id;
    private Venue venue;
    private List<Act> actsPerforming;
    private Date runDate;
    private HashMap<Integer, Double> tierPrices; //int tierID, double £price
    private int duration;

    /**
     *
     * @param runID - id of Run
     * @param runVenue - Venue object of venue run is happening at
     * @param performers - list of Act objects that are performing at run
     * @param date - date of run
     * @param length - duration in mins 
     */
    public EventRun(int runID, Venue runVenue, List<Act> performers, Date date, int length) {
        id = runID;
        venue = runVenue;
        actsPerforming = performers;
        runDate = date;
        duration = length;
    }

    /**
     * Default Constructor
     */
    public EventRun() {
        
    }
    
    /**
     *
     * @return run id
     */
    @Override
    public int getID() {
        return this.id;
    }
    
    /**
     *
     * @param newID - new run id
     */
    @Override
    public void setID(int newID) {
        if(newID >= 0) {
            this.id = newID;    
        } else {
            throw new IllegalArgumentException("ID cannot be an negative integer");
        }
        
    }

    /**
     *
     * @return Venue object run is happening at
     */
    @Override
    public Venue getVenue() {
        return this.venue;
    }

    /**
     *
     * @param newVenue - new Venue object run is happening at
     */
    @Override
    public void setVenue(Venue newVenue) {
        if( null != newVenue) {
            this.venue = newVenue;
        } else {
            throw new NullPointerException("Venue cannot be a null value");
        }
    }

    /**
     *
     * @return list of Acts performing at run
     */
    @Override
    public List<Act> getActsPerforming() {
        List<Act> actsPerformingCopy = new ArrayList();
        actsPerformingCopy.addAll(this.actsPerforming);
        return actsPerformingCopy;
    }

    /**
     *
     * @param performers - new list of Acts performing at run
     */
    @Override
    public void setActsPerforming(List<Act> performers) {
        this.actsPerforming = performers;
    }

    /**
     *
     * @param newAct - new Act object to perform at run
     * @return - boolean if addition was successful
     */
    @Override
    public boolean addActToEvent(Act newAct) {
        boolean blnAdded = false;
        if(newAct != null) {
            if(!this.actsPerforming.contains(newAct)) {
                blnAdded = this.actsPerforming.add(newAct);
            } else {
                throw new IllegalArgumentException("cannot add duplicate acts to list");
            }
        } else {
            throw new NullPointerException("cannot add a NULL object to acts performing list");
        }
        return blnAdded;
    }

    /**
     *
     * @param oldAct - Act object to be removed from run
     * @return boolean if removal was successful
     */
    @Override
    public boolean removeActFromEventRun(Act oldAct) {
        boolean blnRemoved = false;
        if(oldAct != null) {
            if(this.actsPerforming.contains(oldAct)) {
                blnRemoved = this.actsPerforming.remove(oldAct);
            } else {
                throw new IllegalArgumentException("cannot remove objects not in the list");
            }
        } else {
            throw new NullPointerException("cannot remove a NULL object from the list");
        }
        return blnRemoved;
    }

    /**
     *
     * @return date of run
     */
    @Override
    public Date getRunDate() {
        return this.runDate;
    }

    /**
     *
     * @param newRunDate - new date of run
     */ 
    @Override
    public void setRunDate(Date newRunDate) {
        this.runDate = newRunDate;
    }

    /**
     *
     * @return duration in mins
     */
    @Override
    public int getDuration() {
        return this.duration;
    }

    /**
     *
     * @param newDuration - new duration in mins
     */
    @Override
    public void setDuration(int newDuration) {
        if(newDuration > 0) { 
            this.duration = newDuration;
        } else {
            throw new IllegalArgumentException("Cannot set a negative duration");
        }
    }

    @Override
    public int compare(EventRun o1, EventRun o2) {
        return o1.getID() - o2.getID();
    }

}
