package AdministrationApplication;

import AdministrationApplication.Interfaces.IVenue;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author Joel Gooch
 */

public class Venue implements IVenue, Comparator<Venue> {
    
    private int id;
    private String name;
    private Address venueAddress;
    private List<Tier> tierList;
    
    /**
     * default constructor
     */
    public Venue(){
        this.tierList = new ArrayList();
    }
    
    /**
     *
     * @param venueName - name of venue
     * @param venueAddr - Address object of venue
     */
    public Venue(String venueName, Address venueAddr) {
        this.name = venueName;
        this.venueAddress = venueAddr;
        this.tierList = new ArrayList();
    }
    
    /**
     *
     * @return venue id
     */
    @Override
    public int getVenueID() {
        return this.id;
    }
    
    /**
     *
     * @param venueID - new venue id
     */
    @Override
    public void setVenueID(int venueID){
         if(venueID >= 0) {
            this.id = venueID;
        } else {
            throw new IllegalArgumentException("venue id can not be negative");
        }
    }

    /**
     *
     * @return venue name
     */
    @Override
    public String getVenueName() {
        return this.name;
    }

    /**
     *
     * @param newVenueName - new venue name
     */
    @Override
    public void setVenueName(String newVenueName) {
        if(null != newVenueName) {
            if(!newVenueName.isEmpty()) {
                this.name = newVenueName;
            } else {
                throw new IllegalArgumentException("Venue name cannot be an empty string");
            }
        } else {
            throw new NullPointerException("Cannot store a NULL value for Venue name");
        }
    }

    /**
     *
     * @return Address object of venue
     */
    @Override
    public Address getVenueAddress() {
        return this.venueAddress;
    }

    /**
     *
     * @param newVenueAddress - new Address object of venue
     */
    @Override
    public void setVenueAddress(Address newVenueAddress) {
        if(newVenueAddress != null) {
        this.venueAddress = newVenueAddress;
        } else {
            throw new NullPointerException("Address can not be a null value");
        }
    }

    /**
     *
     * @return total capacity of venue among all contained Tiers
     */
    @Override
    public int getCapacity() {
        int capacity = 0;
        for (Tier tier : this.tierList) {
            capacity += tier.getCapacity();
        }
        return capacity;
    }
    
    /**
     *
     * @return total standing capacity of venue among all standing Tiers
     */
    @Override
    public int getStandingCapacity() {
        int standingCapacity = 0;
        for (Tier tier : this.tierList) {
            if (tier.getIsSeated() == false) {
                standingCapacity += tier.getCapacity();
            }
        }
        return standingCapacity;
    }
    
    /**
     *
     * @return total seated capacity of venue among all seated Tiers
     */
    @Override
    public int getSeatedCapacity() {
        int seatedCapacity = 0;
        for (Tier tier : this.tierList) {
            if (tier.getIsSeated() == true) {
                seatedCapacity += tier.getCapacity();
            }
        }
        return seatedCapacity;
    }

    /**
     *
     * @return list of Tier objects within venue
     */
    @Override
    public List<Tier> getTierList() {
        ArrayList<Tier> tierResult = new ArrayList<>();
        for(Tier currTier : this.tierList) {
            tierResult.add(currTier);
        }
        return tierResult;
    }

    /**
     *
     * @param newTier - new Tier object to be added to Venue
     * @return - boolean if addition was successful or not
     */
    @Override
    public boolean addTier(Tier newTier) {
        boolean blnAdded = false;
        if(newTier != null) {
            if(!this.tierList.contains(newTier)) {
                blnAdded = this.tierList.add(newTier);
            } else {
                throw new IllegalArgumentException("cannot add duplicate tiers to list");
            }
        } else {
            throw new NullPointerException("cannot add a NULL object to tierlist");
        }
        return blnAdded;
    }

    /**
     *
     * @param newTiers - list of Tier objects to be added to venue
     * @return boolean if addition was successful or not
     */
    @Override
    public boolean addTiers(List<Tier> newTiers) {
        boolean addSuccessful = false;
        for (Tier tier : newTiers) {
            addSuccessful = this.tierList.add(tier);
            if (addSuccessful = false) {
                break;
            }
        }
        return addSuccessful;
    }
    
    /**
     *
     * @param newTiers list of Tiers to be set as Venue Tiers
     */
    @Override
    public void setTierList(List<Tier> newTiers) {
        this.tierList = newTiers;
    }

    /**
     *
     * @param oldTier - Tier object to be removed from venue
     * @return boolean if removal was successful or not
     */
    @Override
    public boolean removeTier(Tier oldTier) {
        boolean blnRemoved = false;
        if(oldTier != null) {
            if(this.tierList.contains(oldTier)) {
                blnRemoved = this.tierList.add(oldTier);
            } else {
                throw new IllegalArgumentException("cannot remove objects not in the list");
            }
        } else {
            throw new NullPointerException("cannot remove a NULL object from the list");
        }
        return blnRemoved;
    }

    /**
     *
     * @param oldTiers - list of Tier objects to be removed from Venue
     * @return boolean if removal was successful or not
     */
    @Override
    public boolean removeTiers(List<Tier> oldTiers) {
        boolean removeSuccessful = false;
        for (Tier tier : oldTiers) {
            removeSuccessful = this.tierList.remove(tier);
            if (removeSuccessful = false) {
                break;
            }
        }
        return removeSuccessful;
    }

    @Override
    public int compare(Venue o1, Venue o2) {
        return o1.getVenueID() - o2.getVenueID();
    }
}