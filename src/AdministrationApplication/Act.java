package AdministrationApplication;

import AdministrationApplication.Interfaces.IAct;
import java.util.Comparator;

/**
 *
 * @author Joel Gooch
 */

public class Act implements IAct, Comparator<Act> {
    
    private int actID;
    private String actName;
    private String actDesc;
    private EventType actType;
    private int performanceID;
    
    /**
     *
     * @param name - Name for new Act
     * @param desc - Brief description of new act 
     * @param genre - EventType object containing type ID and type name
     */
    public Act(String name, String desc, EventType genre) {
        actName = name;
        actDesc = desc;
        actType = genre;
    }

    /**
     * Default Constructor for Act
     */
    public Act() {

    }

    /**
     *
     * @return Act ID
     */
    @Override
    public int getID() {
        return this.actID;
    }
    
    /**
     *
     * @param newActID - Act ID to be set
     */
    @Override
    public void setID(int newActID) {
        if(newActID >= 0) {
            this.actID = newActID;
        } else {
            throw new IllegalArgumentException("ID cannot be an negative integer");
        }
    }

    /**
     *
     * @return Act Name
     */
    @Override
    public String getActName() {
        return this.actName;
    }

    /**
     *
     * @param newActName - Act name to be set
     */
    @Override
    public void setActName(String newActName) {
        if(null != newActName) {
            if(!newActName.isEmpty()) {
                this.actName = newActName;
            } else {
                throw new IllegalArgumentException("Act name cannot be an empty string");
            }
        } else {
            throw new NullPointerException("Cannot store a NULL value for an act name");
        }
    }
    
    /**
     *
     * @return Brief Act description
     */
    @Override
    public String getActDesc() {
        return this.actDesc;
    }
    
    /**
     *
     * @param newActDesc - new Brief description to be set
     */
    @Override
    public void setActDesc(String newActDesc) {
        if(null != newActDesc) {
            if(!newActDesc.isEmpty()) {
                this.actDesc = newActDesc;
            } else {
                throw new IllegalArgumentException("Description name cannot be an empty string");
            }
        } else {
            throw new NullPointerException("Cannot store a NULL value for an description name");
        }
    }
    
    /**
     *
     * @return Event Type object containing Act type
     */
    @Override
    public EventType getActType() {
        return this.actType;
    }

    /**
     *
     * @param newActType - new EventType object to be set to Act
     */
    @Override
    public void setActType(EventType newActType) {
        if(null != newActType) {
            this.actType = newActType;
        } else {
            throw new NullPointerException("Cannot store a NULL value for an act type");
        }
    }
    
    /**
     *
     * @return Performance ID relating to Performances Table
     */
    public int getPerformanceID() {
        return performanceID;
    }

    /**
     *
     * @param newPerformanceID - new Performance ID relating to Performances Table
     */
    public void setPerformanceID(int newPerformanceID) {
        this.performanceID = newPerformanceID;
    }

    @Override
    public int compare(Act o1, Act o2) {
        return o1.getActName().compareTo(o2.getActName());
    }

    
}
