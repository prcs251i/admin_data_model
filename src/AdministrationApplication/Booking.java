package AdministrationApplication;

import AdministrationApplication.Interfaces.IBooking;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Joel Gooch
 */

public final class Booking implements IBooking, Comparator<Booking> {

    private int bookingID;
    private EventRun run;
    private Customer customer;
    private Date datePlaced;
    private int noOfTickets;
    private List<Ticket> orderedTickets;
    private double totalCost;

    /**
     * Default Constructor
     */
    public Booking() {
        orderedTickets = new ArrayList();
    }
    
    /**
     *
     * @param runBooked - EventRun object of Run booked
     * @param cust - Customer object of Customer who has made booking
     * @param bookingDate - date of booking
     * @param ticketQuantity - number of tickets in booking
     * @param orderCost - total cost of tickets in booking
     */
    public Booking(EventRun runBooked, Customer cust, Date bookingDate, int ticketQuantity, double orderCost) {
        run = runBooked;
        customer = cust;
        datePlaced = bookingDate;
        noOfTickets = ticketQuantity;
        orderedTickets = new ArrayList();
        totalCost = orderCost;
    }

    /**
     *
     * @return
     */
    @Override
    public int getBookingID() {
        return this.bookingID;
    }
    
    /**
     *
     * @param newBookingID
     */
    @Override
    public void setBookingID(int newBookingID) {
        if(newBookingID >= 0) {
            this.bookingID = newBookingID;
        } else {
            throw new IllegalArgumentException("BookingId cannot be an negative number");
        }
    }
    
    /**
     *
     * @return Customer object 
     */
    public Customer getCustomer() {
        return this.customer;
    }
    
    /**
     *
     * @param newCustomer - new Customer Object 
     */
    public void setCustomer(Customer newCustomer) {
        if(newCustomer != null) {
            this.customer = newCustomer;
        } else {
            throw new NullPointerException("Customer cannot be an null value");
        }
    }
    
    /**
     *
     * @return EventRun object for booking made
     */
    @Override
    public EventRun getRun() {
        return this.run;
    }

    /**
     *
     * @param newRun - new EventRun object for booking
     */
    @Override
    public void setRun(EventRun newRun) {
        if(newRun != null) {
            this.run = newRun;
        } else {
            throw new NullPointerException("EventRun cannot be an null value");
        }
    }

    /**
     *
     * @return Date of Booking
     */
    @Override
    public Date getDatePlaced() {
        return this.datePlaced;
    }
    
    /**
     *
     * @param newDatePlaced - new Date of booking
     */
    @Override
    public void setDatePlaced(Date newDatePlaced) {
        this.datePlaced = newDatePlaced;
    }
    
    /**
     *
     * @return no of tickets in booking
     */
    @Override
    public int getNoOfTickets() {
        return this.noOfTickets;
    }
    
    /**
     *
     * @param newNoOfTickets - new number of tickets in booking
     */
    @Override
    public void setNoOfTickets(int newNoOfTickets) {
        if(newNoOfTickets >= 0) {
            this.noOfTickets = newNoOfTickets;
        } else {
            throw new IllegalArgumentException("No of tickets cannot be an negative value");
        }
    }

    /**
     *
     * @return - list of Ticket objects attached to booking
     */
    @Override
    public List<Ticket> getTicketList() {
        List<Ticket> ticketListCopy = new ArrayList();
        ticketListCopy.addAll(this.orderedTickets);
        return ticketListCopy;
    }
    
    /**
     *
     * @param ticketList - new list of Ticket objects for booking
     */
    @Override
    public void setTicketList(List<Ticket> ticketList) {
        this.orderedTickets = ticketList;
    }

    /**
     *
     * @return total cost of all Tickets in Booking
     */
    @Override
    public double getTotalCost() {
        return this.totalCost;
    }
    
    /**
     *
     * @param newTotalCost - new total cost of all Tickets in Booking
     */
    @Override
    public void setTotalCost(double newTotalCost) {
        if(newTotalCost >= 0) {
            this.totalCost = newTotalCost;
        } else {
            throw new IllegalArgumentException("Total cost cannot be an negative value");
        }
    }

    @Override
    public int compare(Booking o1, Booking o2) {
        return o1.getBookingID() - o2.getBookingID();
    }
    
}
