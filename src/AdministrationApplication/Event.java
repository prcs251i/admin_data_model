package AdministrationApplication;

import AdministrationApplication.Interfaces.IEvent;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author Joel Gooch
 */

public class Event implements IEvent, Comparator<Event> {

    private int id;
    private String title;
    private String description;
    private List<EventRun> runList;
    private EventType eventType;
    private double ratingAvg;
    private List<Review> reviews;
    private String age_Rating;
    private String image;

    /**
     * Default Constructor
     */
    public Event() {
    }

    /**
     *
     * @param eventTitle
     * @param eventDescription
     * @param type - EventType object 
     * @param ageRate - age rating of event
     * @param imageLink - URL string of image
     */
    public Event(String eventTitle, String eventDescription, EventType type, String ageRate, String imageLink) {
        title = eventTitle;
        description = eventDescription;
        runList = new ArrayList();
        reviews = new ArrayList();
        eventType = type;
        age_Rating = ageRate;
        image = imageLink;
    }

    /**
     *
     * @return event id
     */
    @Override
    public int getID() {
        return this.id;
    }

    /**
     *
     * @param newId - new event id
     */
    @Override
    public void setID(int newId) {
        if (newId >= 0) {
            this.id = newId;
        } else {
            throw new IllegalArgumentException("EventId can not be a negative number");
        }
    }

    /**
     *
     * @return event title
     */
    @Override
    public String getEventTitle() {
        return this.title;
    }

    /**
     *
     * @param newEventTitle - new event title
     */
    @Override
    public void setEventTitle(String newEventTitle) {
        if (newEventTitle != null) {
            if (!newEventTitle.isEmpty()) {
                this.title = newEventTitle;
            } else {
                throw new IllegalArgumentException("Event title can not be an empty string");
            }
        } else {
            throw new NullPointerException("Event title can not be a null value");
        }
    }

    /**
     *
     * @return event description
     */
    @Override
    public String getEventDescription() {
        return this.description;
    }

    /**
     *
     * @param newEventDescription - new event description
     */
    @Override
    public void setEventDescription(String newEventDescription) {
        if (newEventDescription != null) {
            if (!newEventDescription.isEmpty()) {
                this.description = newEventDescription;
            } else {
                throw new IllegalArgumentException("Event description can not be an empty string");
            }
        } else {
            throw new NullPointerException("Event description can not be a null value");
        }
    }

    /**
     *
     * @return list of EventRun objects for event
     */
    @Override
    public List<EventRun> getEventRuns() {
        List<EventRun> eventRunListCopy = new ArrayList();
        eventRunListCopy.addAll(this.runList);
        return eventRunListCopy;
    }

    /**
     *
     * @param eventRunList - new list of EventRun objects for event
     */
    @Override
    public void setEventRuns(List<EventRun> eventRunList) {
        this.runList = eventRunList;
    }

    /**
     *
     * @return EvenyType object for event
     */
    @Override
    public EventType getEventType() {
        return this.eventType;
    }

    /**
     *
     * @param newType - new EventType object for event
     */
    @Override
    public void setEventType(EventType newType) {
        if (newType != null) {
            this.eventType = newType;
        } else {
            throw new NullPointerException("Event type can not be a null value");
        }
    }

    /**
     *
     * @return average rating calculated from number of reviews and ratings
     */
    @Override
    public double calculateAverageRating() {
        double average = 0.0;
        int noOfReviews = 0;
        for (Review review : this.reviews) {
            noOfReviews++;
            average += review.userRating;
        }
        return average / noOfReviews;
    }

    /**
     *
     * @return average rating of event
     */
    @Override
    public double getAverageRating() {
        return this.ratingAvg;
    }
    
    /**
     *
     * @param newAvgRating - new average rating of event
     */
    @Override
    public void setAverageRating(double newAvgRating) {
        this.ratingAvg = newAvgRating;
    }

    /**
     *
     * @return list of Review object for Event
     */
    @Override
    public List<Review> getReviewsList() {
        List<Review> reviewListCopy = new ArrayList();
        reviewListCopy.addAll(this.reviews);
        return reviewListCopy;
    }

    /**
     *
     * @param reviewList - new list of Review objects for event
     */
    @Override
    public void setReviewsList(List<Review> reviewList) {
        this.reviews = reviewList;
    }

    /**
     *
     * @param newReview - new Review object
     * @return boolean if addition was successful
     */
    @Override
    public boolean addReview(Review newReview) {
        boolean blnAdded = false;
        if (newReview != null) {
            if (!this.reviews.contains(newReview)) {
                blnAdded = this.reviews.add(newReview);
            } else {
                throw new IllegalArgumentException("Can not add a duplicate review");
            }
        } else {
            throw new NullPointerException("Can not add a NULL value as a review");
        }
        return blnAdded;
    }

    /**
     *
     * @param oldReview - Review object to be removed
     * @return
     */
    @Override
    public boolean removeReview(Review oldReview) {
        boolean blnRemoved = false;
        if (oldReview != null) {
            if (this.reviews.contains(oldReview)) {
                blnRemoved = this.reviews.remove(oldReview);
            } else {
                throw new IllegalArgumentException("cannot remove objects not in the list");
            }
        } else {
            throw new NullPointerException("cannot remove a NULL object from the list");
        }
        return blnRemoved;
    }

    /**
     *
     * @return age rating for Event
     */
    @Override
    public String getAgeRating() {
        return this.age_Rating;
    }

    /**
     *
     * @param newAgeRating - new age rating for Event
     */
    @Override
    public void setAgeRating(String newAgeRating) {
        if (newAgeRating != null) {
            if (!newAgeRating.isEmpty()) {
                this.age_Rating = newAgeRating;
            } else {
                throw new IllegalArgumentException("Age rating can not be an empty string");
            }
        } else {
            throw new NullPointerException("Age rating can not be a null value");
        }
    }

    /**
     *
     * @return URL string for image
     */
    @Override
    public String getImageString() {
        return this.image;
    }

    /**
     *
     * @param newImageString - new URL string for image
     */
    @Override
    public void setImageString(String newImageString) {
        if (newImageString != null) {
            if (!newImageString.isEmpty()) {
                this.image = newImageString;
            } else {
                throw new IllegalArgumentException("Image sting can not be an empty string");
            }
        } else {
            throw new NullPointerException("Image string can not be a null valur");
        }
    }

    @Override
    public int compare(Event o1, Event o2) {
        return o1.getID() - o2.getID();
    }

}
