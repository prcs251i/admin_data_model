package AdministrationApplication;

import AdministrationApplication.Interfaces.ITicket;
import java.util.Comparator;

/**
 *
 * @author Joel Gooch
 */

public abstract class Ticket implements ITicket, Comparator<Ticket> {

    private int ticketRef;
    private int bookingID;
    private Tier allocatedTier;

    /**
     * default constructor
     */
    public Ticket() {

    }

    /**
     * Abstract Ticket constructor
     * 
     * @param ref - ticket id
     * @param bookingRef - booking id
     * @param venue - Venue object ticket is valid within
     * @param tier - Tier object ticket is valid within
     */
    public Ticket(int ref, int bookingRef, Venue venue, Tier tier) {
        ticketRef = ref;
        bookingID = bookingRef;
        allocatedTier = tier;
    }

    /**
     *
     * @return ticket id
     */
    @Override
    public int getTicketRef() {
        return this.ticketRef;
    }

    /**
     *
     * @param newTicketRef - new ticket id
     */
    @Override
    public void setTicketRef(int newTicketRef) {
        if(newTicketRef >= 0) {
            this.ticketRef = newTicketRef;
        } else {
            throw new IllegalArgumentException("Ticket ref can not be negative");
        }
    }

    /**
     *
     * @return booking id
     */
    @Override
    public int getBookingID() {
        return this.bookingID;
    }

    /**
     *
     * @param newBookingID - new booking id
     */
    @Override
    public void setBookingID(int newBookingID) {
        if(newBookingID >= 0) {
            this.bookingID = newBookingID;
        } else {
            throw new IllegalArgumentException("Booking id can not be negative");
        }
    }

    /**
     *
     * @return Tier object ticket is valid within
     */
    @Override
    public Tier getAllocatedTier() {
        return this.allocatedTier;
    }

    /**
     *
     * @param newTier - new tier object ticket is valid within
     */
    @Override
    public void setAllocatedTier(Tier newTier) {
        if(newTier != null) {
        this.allocatedTier = newTier;
        } else {
            throw new NullPointerException("Allocated tier can not be a Null value");
        }
    }

    @Override
    public int compare(Ticket o1, Ticket o2) {
        return o1.getTicketRef() - o2.getTicketRef();
    }

}
