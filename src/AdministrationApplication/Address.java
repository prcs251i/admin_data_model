package AdministrationApplication;

import AdministrationApplication.Interfaces.IAddress;

/**
 *
 * @author Joel Gooch
 */

public class Address implements IAddress {

    private String address_Line_1;
    private String address_Line_2;
    private String city;
    private String county;
    private String postcode;
    
    /**
     * Default Constructor
     */
    public Address() {
        
    }
    
    /**
     *
     * @param addrLine1 - Address Line 1
     * @param addrLine2 - Address Line 2
     * @param addrCity - Address City
     * @param addrCounty - Address County
     * @param addrPostcode - Address Postcode
     */
    public Address(String addrLine1, String addrLine2, String addrCity, String addrCounty, String addrPostcode) {
        address_Line_1 = addrLine1;
        address_Line_2 = addrLine2;
        city = addrCity;
        county = addrCounty;
        postcode = addrPostcode;
    }

    /**
     *
     * @return Address Line 1
     */
    @Override
    public String getAddressLine1() {
        return this.address_Line_1;
    }

    /**
     *
     * @param newAddrLine1 - new Address Line 1
     */
    @Override
    public void setAddressLine1(String newAddrLine1) {
         if(null != newAddrLine1) {
            if(!newAddrLine1.isEmpty()) {
                this.address_Line_1 = newAddrLine1;
         } else {
                throw new IllegalArgumentException("AddrLine1 cannot be an empty string");
            }
        } else {
            throw new NullPointerException("Cannot store a NULL value for AddrLine1");
        }
    }

    /**
     *
     * @return Address Line 2
     */
    @Override
    public String getAddressLine2() {
        return this.address_Line_2;
    }

    /**
     *
     * @param newAddrLine2 - new Address Line 2
     */
    @Override
    public void setAddressLine2(String newAddrLine2) {
        if(null != newAddrLine2) {
                this.address_Line_2 = newAddrLine2;
        } else {
            throw new NullPointerException("Cannot store a NULL value for AddrLine2");
        }
    }

    /**
     *
     * @return City
     */
    @Override
    public String getCity() {
        return this.city;
    }

    /**
     *
     * @param newCity - new City
     */
    @Override
    public void setCity(String newCity) {
        if(null != newCity) {
            if(!newCity.isEmpty()) {
                this.city = newCity;
                } else {
                throw new IllegalArgumentException("City cannot be an empty string");
            }
        } else {
            throw new NullPointerException("Cannot store a NULL value for City");
        }
    }

    /**
     *
     * @return County
     */
    @Override
    public String getCounty() {
        return this.county;
    }

    /**
     *
     * @param newCounty - new County
     */
    @Override
    public void setCounty(String newCounty) {
        if(null != newCounty) {
            if(!newCounty.isEmpty()) {
                this.county = newCounty;
                 } else {
                throw new IllegalArgumentException("County cannot be an empty string");
            }
        } else {
            throw new NullPointerException("Cannot store a NULL value for County");
        }
    }

    /**
     *
     * @return Postcode
     */
    @Override
    public String getPostcode() {
       return this.postcode;
    }

    /**
     *
     * @param newPostcode - new Postcode
     */
    @Override
    public void setPostcode(String newPostcode) {
        if(null != newPostcode) {
            if(!newPostcode.isEmpty()) {
                this.postcode = newPostcode;
                } else {
                throw new IllegalArgumentException("Postcode cannot be an empty string");
            }
        } else {
            throw new NullPointerException("Cannot store a NULL value for an Postcode");
        }
    }
    
           
    
}
