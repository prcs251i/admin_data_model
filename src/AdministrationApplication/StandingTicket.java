package AdministrationApplication;

/**
 *
 * @author Joel Gooch
 */

public class StandingTicket extends Ticket {
    
    /**
     * default constructor
     */
    public StandingTicket() {
        
    }
    
    /**
     *
     * @param ref - ticket id
     * @param bookingRef - booking id
     * @param venue - Venue object ticket is valid within
     * @param tier - Tier object ticket is valid within
     */
    public StandingTicket(int ref, int bookingRef, Venue venue, Tier tier) {
        super(ref, bookingRef, venue, tier);
    }


    
}
