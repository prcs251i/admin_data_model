package AdministrationApplication;

import AdministrationApplication.Interfaces.IEventType;
import java.util.Comparator;

/**
 *
 * @author Joel Gooch
 */

public class EventType implements IEventType, Comparator<EventType> {
    
    private int id;
    private String type;
    
    /**
     * Default Constructor
     */
    public EventType() {
        
    }
    
    /**
     *
     * @param eventType - string of type, e.g. Comedy
     */
    public EventType(String eventType) {
        type = eventType;
    }
    
    /**
     *
     * @return event type id
     */
    @Override
    public int getID() {
        return this.id;
    }
    
    /**
     *
     * @param newID - new event type id
     */
    @Override
    public void setID(int newID) {
        if (newID >= 0) {
            this.id = newID;
        } else {
            throw new IllegalArgumentException("EventType ID can not be a negative integer");
        }
    }
    
    /**
     *
     * @return string of event type e.g. Comedy
     */
    @Override
    public String getType() {
        return this.type;
    }
    
    /**
     *
     * @param newType - new string of event type e.g. Comedy
     */
    @Override
    public void setType(String newType) {
        if(newType != null) {
            if(!newType.isEmpty()) {
                this.type = newType;
            } else {
                throw new IllegalArgumentException("EventType can not be an empty string");
            }
        } else {
            throw new NullPointerException("Event type can not be a null value");
        }
    }

    @Override
    public int compare(EventType o1, EventType o2) {
       return o1.getType().compareTo(o2.getType());
    }
    
}
