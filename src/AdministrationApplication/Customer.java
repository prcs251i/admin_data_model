package AdministrationApplication;

import AdministrationApplication.Interfaces.ICustomer;
import java.util.Date;

/**
 *
 * @author Joel Gooch
 */

public class Customer extends Person implements ICustomer {

    private String homePhone;
    private String mobPhone;
    private Address address;
    private String cardType;
    private String cardNumber;
    private Date expiryDate;
    private String securityCode;

    /**
     * Default Constructor
     */
    public Customer() {

    }

    /**
     * Registered Customer Constructor
     * 
     * @param firstName
     * @param secondName
     * @param email
     * @param homeNo
     * @param mobNo
     * @param custAddress - Address Object of customer
     * @param paymentCardType - payment card type, e.g VISA
     * @param cardNo - payment card long digit number
     * @param expDate - payment card expiration date
     * @param secCode - 3 digit security card number
     * @param pass
     */
    public Customer(String firstName, String secondName, String email, String homeNo, String mobNo, Address custAddress, String paymentCardType, String cardNo, Date expDate, String secCode, String pass) {
        super(firstName, secondName, email, pass);
        mobPhone = mobNo;
        homePhone = homeNo;
        address = custAddress;
        cardType = paymentCardType;
        cardNumber = cardNo;
        expiryDate = expDate;
        securityCode = secCode;
    }

    /**
     * Guest Customer Constructor
     * 
     * @param firstName
     * @param secondName
     * @param emailString
     * @param paymentCardType - payment card type, e.g VISA
     * @param cardNo - payment card long digit number
     * @param expDate - payment card expiration date
     * @param secCode - 3 digit security card number
     */
    public Customer(String firstName, String secondName, String emailString, String paymentCardType, String cardNo, Date expDate, String secCode) {
        super(firstName, secondName, emailString);
        cardType = paymentCardType;
        cardNumber = cardNo;
        expiryDate = expDate;
        securityCode = secCode;
    }

    /**
     *
     * @return Address object of Customer
     */
    @Override
    public Address getAddress() {
        return this.address;
    }

    /**
     *
     * @param newAddress - new Address object for Customer
     */
    @Override
    public void setAddress(Address newAddress) {
        if (null != newAddress) {
            this.address = newAddress;
        } else {
            throw new NullPointerException("Cannot store a null value for address");
        }
    }

    /**
     *
     * @return home telephone number
     */
    @Override
    public String getHomeNumber() {
        return this.homePhone;
    }

    /**
     *
     * @param newHomeNo - new home telephone number
     */
    @Override
    public void setHomeNumber(String newHomeNo) {
        this.homePhone = newHomeNo;
    }

    /**
     *
     * @return - mobile telephone number
     */
    @Override
    public String getMobNumber() {
        return this.mobPhone;
    }

    /**
     *
     * @param newMobNo - new mobile telephone number
     */
    @Override
    public void setMobNumber(String newMobNo) {
        this.mobPhone = newMobNo;
    }

    /**
     *
     * @return card type, e.g. VISA
     */
    @Override
    public String getCardType() {
        return this.cardType;
    }

    /**
     *
     * @param newCardType - new card type, e.g. VISA
     */
    @Override
    public void setCardType(String newCardType) {
        if (null != newCardType) {
            if (!newCardType.isEmpty()) {
                this.cardType = newCardType;
            } else {
                throw new IllegalArgumentException("CardType cannot be an empty string");
            }
        } else {
            throw new NullPointerException("Cannot store a NULL value for CardType");
        }
    }

    /**
     *
     * @return long digit card number
     */
    @Override
    public String getCardNumber() {
        return this.cardNumber;
    }

    /**
     *
     * @param newCardNumber - new long digit payment card number
     */
    @Override
    public void setCardNumber(String newCardNumber) {
        if (null != newCardNumber) {
            if (!newCardNumber.isEmpty()) {
                this.cardNumber = newCardNumber;
            } else {
                throw new IllegalArgumentException("Card number cannot be an empty string");
            }
        } else {
            throw new NullPointerException("Cannot store a NULL value for an Card number");
        }
    }

    /**
     *
     * @return expiration date of payment card
     */
    @Override
    public Date getExpiryDate() {
        return this.expiryDate;
    }

    /**
     *
     * @param newExpDate - new expiration date of payment card
     */
    @Override
    public void setExpiryDate(Date newExpDate) {
        this.expiryDate = newExpDate;
    }

    /**
     *
     * @return payment card 3 digit security number
     */
    @Override
    public String getSecurityCode() {
        return this.securityCode;
    }

    /**
     *
     * @param newSecurityCode - new payment card 3 digit security number
     */
    @Override
    public void setSecurityCode(String newSecurityCode) {
        if (null != newSecurityCode) {
            if (!newSecurityCode.isEmpty()) {
                this.securityCode = newSecurityCode;
            } else {
                throw new IllegalArgumentException("Security code cannot be an empty string");
            }
        } else {
            throw new NullPointerException("Cannot store a NULL value for SecurityCode");
        }
    }

}
