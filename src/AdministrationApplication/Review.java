package AdministrationApplication;

import AdministrationApplication.Interfaces.IReview;
import java.util.Comparator;
import java.util.Date;

/**
 *
 * @author Joel Gooch
 */

public class Review implements IReview, Comparator<Review> {

    private int reviewID;
    private int eventID;
    private int userID;
    private Date reviewDate;
    public int userRating;
    private String userComments;

    /**
     * Default Constructor
     */
    public Review() {

    }

    /**
     *
     * @param eventNo - number of event reviewed
     * @param userNo - id number of user the review was from
     * @param date - date of review
     * @param userScore - score user designated from 1-5
     * @param userComment - additional review comments
     */
    public Review(int eventNo, int userNo, Date date, int userScore, String userComment) {
        eventID = eventNo;
        userID = userNo;
        reviewDate = date;
        userRating = userScore;
        userComments = userComment;
    }

    /**
     *
     * @return review id
     */
    @Override
    public int getReviewID() {
        return this.reviewID;
    }

    /**
     *
     * @param newReviewID - new review id
     */
    @Override
    public void setReviewID(int newReviewID) {
        if(newReviewID >= 0) {
            this.reviewID = newReviewID;
        } else {
            throw new IllegalArgumentException("review id can not be negative");
        }
    }

    /**
     *
     * @return user id who placed review
     */
    @Override
    public int getUserID() {
        return this.userID;
    }

    /**
     *
     * @param newUserID - new user id who has placed review
     */
    @Override
    public void setUserID(int newUserID) {
        if(newUserID >= 0) {
            this.userID = newUserID;
        } else {
            throw new IllegalArgumentException("user id can not be negative");
        }
    }

    /**
     *
     * @return event id that has been reviewed
     */
    @Override
    public int getEventID() {
        return this.eventID;
    }

    /**
     *
     * @param newEventID - new id that has been reviewed
     */
    @Override
    public void setEventID(int newEventID) {
        if(newEventID >= 0) {
            this.eventID = newEventID;
        } else {
            throw new IllegalArgumentException("event id can not be negative");
        }
    }

    /**
     *
     * @return date of review
     */
    @Override
    public Date getReviewDate() {
        return this.reviewDate;
    }

    /**
     *
     * @param newReviewDate - new date of review
     */
    @Override
    public void setReviewDate(Date newReviewDate) {
        this.reviewDate = newReviewDate;
    }

    /**
     *
     * @return user score from 1-5
     */
    @Override
    public int getUserRating() {
        return this.userRating;
    }

    /**
     *
     * @param newRating - new user score from 1-5
     */
    @Override
    public void setUserRating(int newRating) {
        if(newRating >= 0) {
            this.userRating = newRating;
        } else {
            throw new IllegalArgumentException("user rating can not be negative");
        }
    }

    /**
     *
     * @return additional review comments
     */
    @Override
    public String getUserComments() {
        return this.userComments;
    }

    /**
     *
     * @param newComments - new additional review comments
     */
    @Override
    public void setUserComments(String newComments) {
        if(newComments != null) {
            this.userComments = newComments;
        } else {
            throw new NullPointerException("User comments can not be a null value");
        }
    }

    @Override
    public int compare(Review o1, Review o2) {
        return o1.getReviewID() - o2.getReviewID();
    }

}
