package AdministrationApplication;

import AdministrationApplication.Interfaces.ISeatedTicket;

/**
 *
 * @author Joel Gooch
 */

public class SeatedTicket extends Ticket implements ISeatedTicket {
    
    private String seatRow;
    private int seatNumber;

    /**
     *
     */
    public SeatedTicket() {
        
    }
    
    /**
     *
     * @param ref - ticket id
     * @param bookingRef - booking id
     * @param venue - Venue object ticket is valid within
     * @param tier - Tier object ticket is valid within
     * @param row - seat row that ticket is valid within
     * @param seatNo - seat number that ticket is valid on
     */
    public SeatedTicket(int ref, int bookingRef, Venue venue, Tier tier, String row, int seatNo) {
        super(ref, bookingRef, venue, tier);
        seatRow = row;
        seatNumber = seatNo;
    }
    
    /**
     *
     * @return seat number
     */
    @Override
    public int getSeatNo() {
        return this.seatNumber;
    }
    
    /**
     *
     * @param newSeatNo - new seat number
     */
    @Override
    public void setSeatNo(int newSeatNo) {
        if(newSeatNo >= 0) {
            this.seatNumber = newSeatNo;
        } else {
            throw new IllegalArgumentException("Seat no can not be a negative");
        }
    }
    
    /**
     *
     * @return seat row
     */
    @Override
    public String getSeatRow() {
        return this.seatRow;
    }
    
    /**
     *
     * @param newSeatRow - new seat row
     */
    @Override
    public void setSeatRow(String newSeatRow) {
        if(newSeatRow != null) {
            if(!newSeatRow.isEmpty()) {
                this.seatRow = newSeatRow;
            } else {
                throw new IllegalArgumentException("SeatRow can not be an empty string");
            }
        } else {
            throw new NullPointerException("SeatRow can not be a null value");
        }
    }

}
