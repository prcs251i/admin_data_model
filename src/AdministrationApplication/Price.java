package AdministrationApplication;

import AdministrationApplication.Interfaces.IPrice;
import java.util.Comparator;

/**
 *
 * @author Joel Gooch
 */

public class Price implements IPrice, Comparator<Price> {

    private int id;
    private int runID;
    private int tierID;
    private double price;
    
    /**
     * default constructor
     */
    public Price() {
        
    }
    
    /**
     *
     * @param iD - price id
     * @param runId - id of run that prices applies to
     * @param tierId - id of tier that price applies to
     * @param thisPrice - value of ticket cost in £
     */
    public Price(int iD, int runId, int tierId, double thisPrice) {
        id = iD;
        runID = runId;
        tierID = tierId;
        price = thisPrice;
    }

    /**
     *
     * @return ticket id
     */
    @Override
    public int getId() {
        return id;
    }

    /**
     *
     * @param id - new ticket id
     */
    @Override
    public void setId(int id) {
        if(id >= 0) {
            this.id = id;
        } else {
            throw new IllegalArgumentException("id can not be a negative number");
        }
    }

    /**
     *
     * @return run id that price applies to
     */
    @Override
    public int getRunID() {
        return runID;
    }

    /**
     *
     * @param runID - new run id that price applies to
     */
    @Override
    public void setRunID(int runID) {
        if(runID >= 0) {
            this.runID = runID;
        } else {
            throw new IllegalArgumentException("run id can not be a negative number");
        }
    }

    /**
     *
     * @return tier id that price applies to
     */
    @Override
    public int getTierID() {
        return tierID;
    }

    /**
     *
     * @param tierID - new tier id that price applies to
     */
    @Override
    public void setTierID(int tierID) {
        if(tierID >= 0) {
            this.tierID = tierID;
        } else {
            throw new IllegalArgumentException("tierid can not be a negative number");
        }
    }

    /**
     *
     * @return price of ticket in £
     */
    @Override
    public double getPrice() {
        return price;
    }

    /**
     *
     * @param price - new price of ticket in £
     */
    @Override
    public void setPrice(double price) {
        if(price >= 0) {
            this.price = price;
        } else {
            throw new IllegalArgumentException("price can not be a negative number");
        }
    }

    @Override
    public int compare(Price o1, Price o2) {
        return o1.getId() - o2.getId();
    }

}
