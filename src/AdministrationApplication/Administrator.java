package AdministrationApplication;

/**
 *
 * @author Joel Gooch
 */

public class Administrator extends Person {

    /**
     * Default Constructor
     */
    public Administrator() {

    }

    /**
     *
     * @param forename
     * @param surname
     * @param email
     * @param pass
     */
    public Administrator(String forename, String surname, String email, String pass) {
        super(forename, surname, email, pass);
    }

}
