package AdministrationApplication;

import AdministrationApplication.Interfaces.ITier;
import java.util.Comparator;

/**
 *
 * @author Joel Gooch
 */

public class Tier implements ITier, Comparator<Tier> {

    private int tierId;
    private int venueId;
    private String tierName;
    private boolean seated;
    private int capacity;
    private Price ticketCost;
    private int seatRows;
    private int seatColumns;

    /**
     * default constructor
     */
    public Tier() {

    }

    /**
     *
     * @param name - name of tier
     * @param isSeated - whether tier is standing or seated based (exclusive)
     * @param totalCapacity - total capacity of Tier
     * @param noOfSeatRows - number of seat rows within Tier
     * @param noOfSeatColumns - number of seat columns within Tier
     */
    public Tier(String name, boolean isSeated, int totalCapacity, int noOfSeatRows, int noOfSeatColumns) {
        this.tierName = name;
        this.seated = isSeated;
        this.capacity = totalCapacity;
        this.seatRows = noOfSeatRows;
        this.seatColumns = noOfSeatColumns;
    }

    /**
     *
     * @return tier id
     */
    @Override
    public int getTierId() {
        return this.tierId;
    }

    /**
     *
     * @param newTierId - new tier id
     */
    @Override
    public void setTierId(int newTierId) {
        if(newTierId >= 0) {
            this.tierId = newTierId;
        } else {
            throw new IllegalArgumentException("tier id can not be negative");
        }
    }

    /**
     *
     * @return venue id
     */
    @Override
    public int getVenueId() {
        return this.venueId;
    }

    /**
     *
     * @param newVenueId - new venue id
     */
    @Override
    public void setVenueId(int newVenueId) {
        if(newVenueId >= 0) {
            this.venueId = newVenueId;
        } else {
            throw new IllegalArgumentException("venue id can not be negative");
        }
    }

    /**
     *
     * @return boolean if seated or not, seated = true or false
     */
    @Override
    public boolean getIsSeated() {
        return this.seated;
    }

    /**
     *
     * @param newIsSeated - new boolean value of whether Tier is seated
     */
    @Override
    public void setIsSeated(boolean newIsSeated) {
        this.seated = newIsSeated;
    }

    /**
     *
     * @return total capacity of Tier
     */
    @Override
    public int getCapacity() {
        return this.capacity;
    }

    /**
     *
     * @param newCapacity - new total capacity of Tier
     */
    @Override
    public void setCapacity(int newCapacity) {
        if(newCapacity >= 0) {
            this.capacity = newCapacity;
        } else {
            throw new IllegalArgumentException("capacity can not be negative");
        }
    }

    /**
     *
     * @return number of rows of seating
     */
    @Override
    public int getSeatRows() {
        return this.seatRows;
    }

    /**
     *
     * @param newSeatRows - new number of rows of seating
     */
    @Override
    public void setSeatRows(int newSeatRows) {
        if(newSeatRows >= 0) {
            this.seatRows = newSeatRows;
        } else {
            throw new IllegalArgumentException("Seat rows can not be negative");
        }
    }

    /**
     *
     * @return number of columns of seating
     */
    @Override
    public int getSeatColumns() {
        return this.seatColumns;
    }

    /**
     *
     * @param newSeatColumns - new number of columns of seating
     */
    @Override
    public void setSeatColumns(int newSeatColumns) {
        if(newSeatColumns >= 0) {
            this.seatColumns = newSeatColumns;
        } else {
            throw new IllegalArgumentException("Seat columns can not be negative");
        }
    }

    /**
     *
     * @return name of Tier
     */
    @Override
    public String getTierName() {
        return this.tierName;
    }

    /**
     *
     * @param newTierName - new name of Tier
     */
    @Override
    public void setTierName(String newTierName) {
        if(null != newTierName) {
            if(!newTierName.isEmpty()) {
                this.tierName = newTierName;
            } else {
                throw new IllegalArgumentException("Tier name cannot be an empty string");
            }
        } else {
            throw new NullPointerException("Cannot store a NULL value for Tier name");
        }
    }

    /**
     *
     * @return Price object of ticket within Tier
     */
    @Override
    public Price getTierTicketCost() {
        return this.ticketCost;
    }

    /**
     *
     * @param newTicketCost - new Price object to set for tier
     */
    @Override
    public void setTierTicketCost(Price newTicketCost) {
        if(newTicketCost != null) {
            this.ticketCost = newTicketCost;
        } else {
            throw new NullPointerException("Ticket cost can not be null");
        }
    }

    @Override
    public int compare(Tier o1, Tier o2) {
        return o1.getTierId() - o2.getTierId();
    }

}
