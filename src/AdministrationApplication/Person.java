package AdministrationApplication;

import AdministrationApplication.Interfaces.IPerson;
import java.util.Comparator;

/**
 *
 * @author Joel Gooch
 */

public abstract class Person implements IPerson, Comparator<Person> {

    private int id;
    private String forename;
    private String surname;
    private String eMail;
    private String password;

    /**
     * Default constructor
     */
    public Person() {

    }

    /**
     * abstract Constructor
     * 
     * @param firstName
     * @param lastName
     * @param eMailAdd
     * @param pass
     */
    public Person(String firstName, String lastName, String eMailAdd, String pass) {
        forename = firstName;
        surname = lastName;
        eMail = eMailAdd;
        password = pass;
    }

    /**
     *
     * @param firstName
     * @param lastName
     * @param eMailAdd
     */
    public Person(String firstName, String lastName, String eMailAdd) {
        forename = firstName;
        surname = lastName;
        eMail = eMailAdd;
    }

    /**
     *
     * @return person id number
     */
    @Override
    public int getID() {
        return this.id;
    }

    /**
     *
     * @param newId - person id number
     */
    @Override
    public void setID(int newId) {
        if (newId >= 0) {
            this.id = newId;
        } else {
            throw new IllegalArgumentException("ID can not be a negative integer");
        }
    }

    /**
     *
     * @return
     */
    @Override
    public String getForename() {
        return this.forename;
    }

    /**
     *
     * @param newForename
     */
    @Override
    public void setForename(String newForename) {
        if (null != newForename) {
            if (!newForename.isEmpty()) {
                this.forename = newForename;
            } else {
                throw new IllegalArgumentException("forename cannot be an empty string");
            }
        } else {
            throw new NullPointerException("Cannot store a NULL value for forename");
        }
    }

    /**
     *
     * @return
     */
    @Override
    public String getSurname() {
        return this.surname;
    }

    /**
     *
     * @param newSurname
     */
    @Override
    public void setSurname(String newSurname) {
        if (null != newSurname) {
            if (!newSurname.isEmpty()) {
                this.surname = newSurname;
            } else {
                throw new IllegalArgumentException("surname cannot be an empty string");
            }
        } else {
            throw new NullPointerException("Cannot store a NULL value for surname");
        }
    }

    /**
     *
     * @return
     */
    @Override
    public String getEmail() {
        return this.eMail;
    }

    /**
     *
     * @param newEmail
     */
    @Override
    public void setEmail(String newEmail) {
        if (null != newEmail) {
            if (!newEmail.isEmpty()) {
                this.eMail = newEmail;
            } else {
                throw new IllegalArgumentException("email cannot be an empty string");
            }
        } else {
            throw new NullPointerException("Cannot store a NULL value for email");
        }
    }

    /**
     *
     * @return
     */
    @Override
    public String getPassword() {
        return this.password;
    }

    /**
     *
     * @param newPass
     */
    @Override
    public void setPassword(String newPass) {
        if (null != newPass) {
            if (!newPass.isEmpty()) {
                this.password = newPass;
            } else {
                throw new IllegalArgumentException("password cannot be an empty string");
            }
        } else {
            throw new NullPointerException("Cannot store a NULL value for password");
        }
    }

    @Override
    public int compare(Person o1, Person o2) {
        return o1.getID() - o2.getID();
    }

}
