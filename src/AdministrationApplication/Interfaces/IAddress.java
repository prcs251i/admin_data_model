package AdministrationApplication.Interfaces;

/**
 *
 * @author Joel Gooch
 */

public interface IAddress {
    
    /**
     *
     * @return
     */
    String getAddressLine1();
    
    /**
     *
     * @param newAddrLine1
     */
    void setAddressLine1(String newAddrLine1);
    
    /**
     *
     * @return
     */
    String getAddressLine2();
    
    /**
     *
     * @param newAddrLine2
     */
    void setAddressLine2(String newAddrLine2);
    
    /**
     *
     * @return
     */
    String getCity();
    
    /**
     *
     * @param newCity
     */
    void setCity(String newCity);
    
    /**
     *
     * @return
     */
    String getCounty();
    
    /**
     *
     * @param newCounty
     */
    void setCounty(String newCounty);
    
    /**
     *
     * @return
     */
    String getPostcode();
    
    /**
     *
     * @param newPostcode
     */
    void setPostcode(String newPostcode);
    
    
}
