package AdministrationApplication.Interfaces;

import AdministrationApplication.Address;
import AdministrationApplication.Tier;
import java.util.List;

/**
 *
 * @author Joel Gooch
 */

public interface IVenue {
    
    /**
     *
     * @return id of venue
     */
    int getVenueID();
    
    /**
     *
     * @param venueID - new id of venue
     */
    void setVenueID(int venueID);
    
    /**
     *
     * @return name of venue
     */
    String getVenueName();
    
    /**
     *
     * @param newVenueName - new venue name
     */
    void setVenueName(String newVenueName);
    
    /**
     *
     * @return Address object of venue
     */
    Address getVenueAddress();
    
    /**
     *
     * @param newVenueAddress - new Address object of venue
     */
    void setVenueAddress(Address newVenueAddress);
    
    /**
     *
     * @return total capacity of venue
     */
    int getCapacity();
    
    /**
     *
     * @return - standing capacity of venue
     */
    int getStandingCapacity();
    
    /**
     *
     * @return seated capacity of venue
     */
    int getSeatedCapacity();
    
    /**
     *
     * @return list of Tier objects within Venue
     */
    List<Tier> getTierList();
    
    /**
     *
     * @param newTier - new Tier object to be added to Venue
     * @return boolean if addition was successful or not
     */
    boolean addTier(Tier newTier);
    
    /**
     *
     * @param newTiers - list of Tier objects to be added to venue
     * @return boolean if addition was successful or not
     */
    boolean addTiers(List<Tier> newTiers);
    
    /**
     *
     * @param newTiers - list of Tier objects to be set as tier list
     */
    void setTierList(List<Tier> newTiers);
    
    /**
     *
     * @param oldTier - Tier object to be removed from venue
     * @return boolean if removal was successful
     */
    boolean removeTier(Tier oldTier);
    
    /**
     *
     * @param oldTiers - list of Tier objects to be removed from venue
     * @return boolean if removal was successful or not
     */
    boolean removeTiers(List<Tier> oldTiers);
    
}
