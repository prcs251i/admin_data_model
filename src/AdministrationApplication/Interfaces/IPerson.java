package AdministrationApplication.Interfaces;

/**
 *
 * @author Joel Gooch
 */

public interface IPerson {
    
    /**
     *
     * @return person id
     */
    int getID();
    
    /**
     *
     * @param newId - new person id
     */
    void setID(int newId);
    
    /**
     *
     * @return
     */
    String getForename();
    
    /**
     *
     * @param newForename 
     */
    void setForename(String newForename);
    
    /**
     *
     * @return
     */
    String getSurname();
    
    /**
     *
     * @param newSurname
     */
    void setSurname(String newSurname);
    
    /**
     *
     * @return
     */
    String getEmail();
    
    /**
     *
     * @param newEmail
     */
    void setEmail(String newEmail);
    
    /**
     *
     * @return
     */
    String getPassword();
    
    /**
     *
     * @param newPass
     */
    void setPassword(String newPass);
}
