package AdministrationApplication.Interfaces;

import AdministrationApplication.*;

/**
 *
 * @author Joel Gooch
 */

public interface ITicket {
    
    /**
     *
     * @return ticket id
     */
    int getTicketRef();
    
    /**
     *
     * @param newTicketRef - new ticket id
     */
    void setTicketRef(int newTicketRef);
    
    /**
     *
     * @return booking id
     */
    int getBookingID();
    
    /**
     *
     * @param newBookingID - new booking id
     */
    void setBookingID(int newBookingID);
    
    /**
     *
     * @return Tier object ticket is within
     */
    Tier getAllocatedTier();
    
    /**
     *
     * @param newTier - new Tier object ticket is within
     */
    void setAllocatedTier(Tier newTier);
}
