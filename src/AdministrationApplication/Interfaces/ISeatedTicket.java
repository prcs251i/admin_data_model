package AdministrationApplication.Interfaces;

/**
 *
 * @author Joel Gooch
 */

public interface ISeatedTicket {
    
    /**
     *
     * @return seat number
     */
    int getSeatNo();
    
    /**
     *
     * @param newSeatRow - new seat number
     */
    void setSeatNo(int newSeatRow);
    
    /**
     * 
     * @return seat row
     */
    String getSeatRow();
    
    /**
     *
     * @param newSeatRow - new seat row
     */
    void setSeatRow(String newSeatRow);
}
