package AdministrationApplication.Interfaces;

import AdministrationApplication.EventType;

/**
 *
 * @author Joel Gooch
 */

public interface IAct {
    
    /**
     *
     * @return act ID
     */
    int getID();
    
    /**
     *
     * @param newActID - new act id
     */
    void setID(int newActID);
    
    /**
     *
     * @return act name
     */
    String getActName();
    
    /**
     *
     * @param newActName - new act name
     */
    void setActName(String newActName);
    
    /**
     *
     * @return act description
     */
    String getActDesc();
    
    /**
     *
     * @param newActDesc - new act description
     */
    void setActDesc(String newActDesc);
    
    /**
     *
     * @return - EventType object for type of act
     */
    EventType getActType();
    
    /**
     *
     * @param newActType - new EventType object for type of act
     */
    void setActType(EventType newActType);
    
}
