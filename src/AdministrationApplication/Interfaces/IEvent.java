package AdministrationApplication.Interfaces;

import AdministrationApplication.EventRun;
import AdministrationApplication.EventType;
import AdministrationApplication.Review;
import java.util.List;

/**
 *
 * @author Joel Gooch
 */

public interface IEvent {
    
    /**
     *
     * @return event id
     */
    int getID();
    
    /**
     *
     * @param newId - new event id
     */
    void setID(int newId);
    
    /**
     *
     * @return event description
     */
    String getEventDescription();
    
    /**
     *
     * @param newEventDescription - new event description
     */
    void setEventDescription(String newEventDescription);
    
    /**
     *
     * @return event title
     */
    String getEventTitle();
    
    /**
     *
     * @param newEventTitle - new event title
     */
    void setEventTitle(String newEventTitle);

    /**
     *
     * @return EventType object for event
     */
    EventType getEventType();
    
    /**
     *
     * @param newGenre - new EventType object for event
     */
    void setEventType(EventType newGenre);
    
    /**
     *
     * @return List of EventRun objects in Event
     */
    List<EventRun> getEventRuns();
    
    /**
     *
     * @param eventRunList - new List of EventRun objects in Event
     */
    void setEventRuns(List<EventRun> eventRunList);
    
    /**
     *
     * @return average user rating of event after calculation
     */
    double calculateAverageRating();
    
    /**
     *
     * @return average user rating of event
     */
    double getAverageRating();
    
     /**
     *
     * @param newAvgRating - new average user rating of event
     */
    void setAverageRating(double newAvgRating);
    
    /**
     *
     * @return list of Review objects in Event
     */
    List<Review> getReviewsList();
    
    /**
     *
     * @param reviewList - new list of Review objects in Event
     */
    void setReviewsList(List<Review> reviewList);
    
    /**
     *
     * @param newReview - new Review object to be added to event
     * @return
     */
    boolean addReview(Review newReview);
    
    /**
     *
     * @param oldReview - old Review object to be removed from event
     * @return
     */
    boolean removeReview(Review oldReview);
    
    /**
     *
     * @return age rating of event
     */
    String getAgeRating();
    
    /**
     *
     * @param newAgeRating - new age rating of event
     */
    void setAgeRating(String newAgeRating);
    
    /**
     *
     * @return URL string of image 
     */
    String getImageString();
    
    /**
     *
     * @param newImageString - new URL string of image 
     */
    void setImageString(String newImageString);
    
}
