package AdministrationApplication.Interfaces;

import AdministrationApplication.Price;

/**
 *
 * @author Joel Gooch
 */

public interface ITier {
    
    /**
     *
     * @return tier id
     */
    int getTierId();
    
    /**
     *
     * @param NewTierId - new tier id
     */
    void setTierId(int NewTierId);
    
    /**
     *
     * @return - venue id tier is within
     */
    int getVenueId();
    
    /**
     *
     * @param NewVenueId - new venue id tier is within
     */
    void setVenueId(int NewVenueId);
    
    /**
     *
     * @return if tier is seated or not
     */
    boolean getIsSeated();
    
    /**
     *
     * @param newIsSeated new value of if tier is seated or not
     */
    void setIsSeated(boolean newIsSeated);
    
    /**
     *
     * @return capacity of tier
     */
    int getCapacity();
    
    /**
     *
     * @param newCapacity - new capacity of tier
     */
    void setCapacity(int newCapacity);
    
    /**
     *
     * @return number of seat rows
     */
    int getSeatRows();
    
    /**
     *
     * @param newSeatRows - new number of seat rows
     */
    void setSeatRows(int newSeatRows);
    
    /**
     *
     * @return number of seat columns
     */
    int getSeatColumns();
    
    /**
     *
     * @param newSeatColumns - new  number of seat columns
     */
    void setSeatColumns(int newSeatColumns);
    
    /**
     *
     * @return name of tier
     */
    String getTierName();
    
    /**
     *
     * @param newTierName - new tier name
     */
    void setTierName(String newTierName);
    
    /**
     *
     * @return Price object of ticket in tier
     */
    Price getTierTicketCost();
    
    /**
     *
     * @param newTicketCost - new Price object of ticket in tier
     */
    void setTierTicketCost(Price newTicketCost);
    
}
