package AdministrationApplication.Interfaces;

import AdministrationApplication.Act;
import AdministrationApplication.Venue;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Joel Gooch
 */

public interface IEventRun {

    /**
     *
     * @return run id
     */
    int getID();
    
    /**
     *
     * @param newID - new run id
     */
    void setID(int newID);
    
    /**
     *
     * @return Venue object of run
     */
    Venue getVenue();

    /**
     *
     * @param newVenue - new Venue object of run
     */
    void setVenue(Venue newVenue);

    /**
     *
     * @return list of Act objects performing at run
     */
    List<Act> getActsPerforming();

    /**
     *
     * @param performers - new list of Act objects performing at run
     */
    void setActsPerforming(List<Act> performers);

    /**
     *
     * @param newAct new Act object to be added to run
     * @return
     */
    boolean addActToEvent(Act newAct);

    /**
     *
     * @param oldAct old Act object to be removed from run
     * @return
     */
    boolean removeActFromEventRun(Act oldAct);

    /**
     *
     * @return date of run
     */
    Date getRunDate();

    /**
     *
     * @param newRunDate - new date of run
     */
    void setRunDate(Date newRunDate);

    /**
     *
     * @return duration of run in mins
     */
    int getDuration();

    /**
     *
     * @param newDuration - new duration of run in mins
     */
    void setDuration(int newDuration);

}
