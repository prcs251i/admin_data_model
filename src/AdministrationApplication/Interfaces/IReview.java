package AdministrationApplication.Interfaces;

import java.util.Date;

/**
 *
 * @author Joel Gooch
 */

public interface IReview {
    
    /**
     *
     * @return review id
     */
    int getReviewID();
    
    /**
     *
     * @param newReviewID - new review id
     */
    void setReviewID(int newReviewID);
    
    /**
     *
     * @return user id
     */
    int getUserID();
    
    /**
     *
     * @param newUserID - new user id
     */
    void setUserID(int newUserID);
    
    /**
     *
     * @return event id
     */
    int getEventID();
    
    /**
     *
     * @param newEventID - new event id
     */
    void setEventID(int newEventID);
    
    /**
     *
     * @return date of review
     */
    Date getReviewDate();
    
    /**
     *
     * @param newReviewDate - new date of review
     */
    void setReviewDate(Date newReviewDate);
    
    /**
     *
     * @return user score from 1-5
     */
    int getUserRating();
    
    /**
     *
     * @param newRating - new user score from 1-5
     */
    void setUserRating(int newRating);
    
    /**
     *
     * @return additional user comments
     */
    String getUserComments();
    
    /**
     *
     * @param newComments - new additional user comments
     */
    void setUserComments(String newComments);
    
}
