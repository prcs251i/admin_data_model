package AdministrationApplication.Interfaces;

/**
 *
 * @author Joel Gooch
 */

public interface IPrice {
 
    /**
     *
     * @return price id
     */
    int getId();
    
    /**
     *
     * @param id - new price id
     */
    void setId(int id);
    
    /**
     *
     * @return run id
     */
    int getRunID();
    
    /**
     *
     * @param runID - new run id
     */
    void setRunID(int runID);
    
    /**
     *
     * @return tier id
     */
    int getTierID();
    
    /**
     *
     * @param tierID - new tier id
     */
    void setTierID(int tierID);
    
    /**
     *
     * @return Price object of ticket
     */
    double getPrice();
    
    /**
     *
     * @param Price - new Price object of ticket
     */
    void setPrice(double Price);
}
