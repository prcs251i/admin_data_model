package AdministrationApplication.Interfaces;

import AdministrationApplication.*;
import java.util.Date;

/**
 *
 * @author Joel Gooch
 */

public interface ICustomer {
    
    /**
     *
     * @return Address object of customer
     */
    Address getAddress();
    
    /**
     *
     * @param newAddress - new Address object of customer
     */
    void setAddress(Address newAddress);
    
    /**
     *
     * @return home phone number of customer
     */
    String getHomeNumber();
    
    /**
     *
     * @param newHomeNo - new home phone number of customer
     */
    void setHomeNumber(String newHomeNo);
    
    /**
     *
     * @return mobile number of customer
     */
    String getMobNumber();
    
    /**
     *
     * @param newMobNo - new mobile number of customer
     */
    void setMobNumber(String newMobNo);
    
    /**
     *
     * @return payment card type of customer e.g. VISA
     */
    String getCardType();
    
    /**
     *
     * @param newCardType - new payment card type of customer e.g. VISA
     */
    void setCardType(String newCardType);
    
    /**
     *
     * @return payment card number
     */
    String getCardNumber();
    
    /**
     *
     * @param newCardNumber - new payment card number
     */
    void setCardNumber(String newCardNumber);
    
    /**
     *
     * @return payment card expiry date
     */
    Date getExpiryDate();
    
    /**
     *
     * @param newExpDate - new payment card expiry date
     */
    void setExpiryDate(Date newExpDate);
    
    /**
     *
     * @return payment card security code
     */
    String getSecurityCode();
    
    /**
     *
     * @param newSecurityCode - new payment card security code
     */
    void setSecurityCode(String newSecurityCode);
    
    
}
