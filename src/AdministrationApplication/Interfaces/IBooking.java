package AdministrationApplication.Interfaces;

import AdministrationApplication.EventRun;
import AdministrationApplication.Ticket;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Joel Gooch
 */

public interface IBooking {
    
    /**
     *
     * @return booking id
     */
    int getBookingID();
    
    /**
     *
     * @param newBookingID - new booking id
     */
    void setBookingID(int newBookingID);
    
    /**
     *
     * @return date of booking
     */
    Date getDatePlaced();
    
    /**
     *
     * @param newDatePlaced - new date of booking
     */
    void setDatePlaced(Date newDatePlaced);
    
    /**
     *
     * @return EventRun object of booking
     */
    EventRun getRun();

    /**
     *
     * @param newRun - new EventRun object of booking
     */
    void setRun(EventRun newRun);
    
    /**
     *
     * @return number of tickets within booking
     */
    int getNoOfTickets();
    
    /**
     *
     * @param newNoOfTickets - new number of tickets within booking
     */
    void setNoOfTickets(int newNoOfTickets);
    
    /**
     *
     * @return list of Ticket objects in booking
     */
    List<Ticket> getTicketList();
    
    /**
     *
     * @param ticketList - new list of Ticket objects in booking
     */
    void setTicketList(List<Ticket> ticketList);
    
    /**
     *
     * @return total cost of tickets in booking
     */
    double getTotalCost();
    
    /**
     *
     * @param newTotalCost - new total cost of tickets in booking
     */
    void setTotalCost(double newTotalCost);
    
}
