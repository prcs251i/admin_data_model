package AdministrationApplication.Interfaces;

/**
 *
 * @author Joel Gooch
 */

public interface IEventType {
    
    /**
     *
     * @return event type id
     */
    int getID();
    
    /**
     *
     * @param newID - new event type id
     */
    void setID(int newID);
    
    /**
     *
     * @return event type e.g. comedy
     */
    String getType();
    
    /**
     *
     * @param newType - new event type e.g. comedy
     */
    void setType(String newType);
}
