package AdministrationApplication;

import AdministrationApplication.Interfaces.IBooking;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Joel Gooch
 */

public class GuestBooking implements IBooking, Comparator<GuestBooking> {

    private int bookingID;
    private EventRun run;
    private String firstName;
    private String lastName;
    private String email;
    private Date datePlaced;
    private int noOfTickets;
    private List<Ticket> orderedTickets;
    private double totalCost;

    /**
     * Default constructor
     */
    public GuestBooking() {
        orderedTickets = new ArrayList();
    }

    /**
     *
     * @param runBooked - EventRun object booked
     * @param firstName - guest first name
     * @param lastName - guest last name
     * @param eMail - guest email
     * @param bookingDate - date of booking
     * @param ticketQuantity - number of tickets in booking
     * @param orderCost - total cost of all tickets in booking
     */
    public GuestBooking(EventRun runBooked, String firstName, String lastName, String eMail, Date bookingDate, int ticketQuantity, double orderCost) {
        run = runBooked;
        email = eMail;
        datePlaced = bookingDate;
        noOfTickets = ticketQuantity;
        orderedTickets = new ArrayList();
        totalCost = orderCost;
    }

    /**
     *
     * @return  booking id
     */
    @Override
    public int getBookingID() {
        return this.bookingID;
    }
    
    /**
     *
     * @param newBookingID - booking id
     */
    @Override
    public void setBookingID(int newBookingID) {
        if(newBookingID >= 0) {
            this.bookingID = newBookingID;
        } else {
            throw new IllegalArgumentException("BookingId can not be an negative number");
        }
    }

    /**
     *
     * @return first name of guest 
     */
    public String getFirstName() {
        return this.firstName;
    }

    /**
     *
     * @param foreName - new first name of guest
     */
    public void setFirstName(String foreName) {
        this.firstName = foreName;
    }

    /**
     *
     * @return last name of guest
     */
    public String getLastName() {
        return this.lastName;
    }

    /**
     *
     * @param surName - new last name of guest
     */
    public void setLastName(String surName) {
        this.lastName = surName;
    }

    /**
     *
     * @return email of guest
     */
    public String getEmail() {
        return this.email;
    }

    /**
     *
     * @param newEmail - new email of guest
     */
    public void setEmail(String newEmail) {
        if(newEmail != null) {
            if(!newEmail.isEmpty()) {
                this.email = newEmail;
            } else {
                throw new IllegalArgumentException("Email can not be an empty string");
            }
        } else {
            throw new NullPointerException("Email can not be NULL");
        }
    }

    /**
     *
     * @return EventRun object of run booked
     */
    @Override
    public EventRun getRun() {
        return this.run;
    }

    /**
     *
     * @param newRun - new EventRun object booked
     */
    @Override
    public void setRun(EventRun newRun) {
        if(newRun != null) {
            this.run = newRun;
        } else {
            throw new NullPointerException("EventRun cannot be an null value");
        }
    }

    /**
     *
     * @return date of booking
     */
    @Override
    public Date getDatePlaced() {
        return this.datePlaced;
    }
    
    /**
     *
     * @param newDatePlaced - new date of booking
     */
    @Override
    public void setDatePlaced(Date newDatePlaced) {
        this.datePlaced = newDatePlaced;
    }
    
    /**
     *
     * @return number of tickets in booking
     */
    @Override
    public int getNoOfTickets() {
        return this.noOfTickets;
    }
    
    /**
     *
     * @param newNoOfTickets - new number of tickets in booking
     */
    @Override
    public void setNoOfTickets(int newNoOfTickets) {
        if(newNoOfTickets > 0) {
            this.noOfTickets = newNoOfTickets;
        } else {
            throw new IllegalArgumentException("No of tickets cannot be an negative value");
        }
    }

    /**
     *
     * @return list of Ticket objects in booking
     */
    @Override
    public List<Ticket> getTicketList() {
        List<Ticket> ticketListCopy = new ArrayList();
        ticketListCopy.addAll(this.orderedTickets);
        return ticketListCopy;
    }
    
    /**
     *
     * @param ticketList - new list of Ticket object in booking
     */
    @Override
    public void setTicketList(List<Ticket> ticketList) {
        this.orderedTickets = ticketList;
    }

    /**
     *
     * @return total cost of all tickets in booking
     */
    @Override
    public double getTotalCost() {
        return this.totalCost;
    }
    
    /**
     *
     * @param newTotalCost - new total cost of all tickets in booking
     */
    @Override
    public void setTotalCost(double newTotalCost) {
        if(newTotalCost > 0) {
            this.totalCost = newTotalCost;
        } else {
            throw new IllegalArgumentException("Total cost cannot be an negative value");
        }
    }

    @Override
    public int compare(GuestBooking o1, GuestBooking o2) {
        return o1.getBookingID() - o2.getBookingID();
    }

}
